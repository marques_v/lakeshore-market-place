package luc.persistence;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.mysql.jdbc.Connection;
import java.util.Properties;

public class DataBase {
	private Connection connection;
	private Properties prop = new Properties();
	
	// Database info
	private static String driver;
	private static String url;
	private static String user;
	private static String password;

	public DataBase() throws IOException{
		System.out.println(System.getProperty("user.dir"));
		prop.load(new FileInputStream("dbconnection.properties"));
		driver = prop.getProperty("driver");
		url = prop.getProperty("url");
		user = prop.getProperty("user");
		password = prop.getProperty("password");
	}
	
	public String getDriver(){
		return driver;
	}
	
    public Connection openConnection() {
        try {
            Class.forName(driver);
            connection = (Connection) DriverManager.getConnection(url,user,password);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
        return connection;
    }

    public void closeConnection() {
        try {
        	connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(DataBase.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
