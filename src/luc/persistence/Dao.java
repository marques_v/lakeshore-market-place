package luc.persistence;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;


public interface Dao {
	public abstract boolean insert(Object o) throws SQLException;
    public abstract boolean edit(Object o) throws SQLException;
    public abstract boolean remove(Object o) throws SQLException;
    public abstract Object consult(Object o) throws SQLException;
    public abstract List getAll() throws SQLException;
}
