package luc.home.service;

import luc.home.service.representation.HomeRepresentation;

public interface HomeService {
	
	HomeRepresentation getHomePage();
}
