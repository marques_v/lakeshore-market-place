package luc.home.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import luc.home.service.representation.HomeRepresentation;
import luc.home.service.workflow.HomeActivity;

@Path("/marketservice/")
public class HomeResource implements HomeService{

	@Override
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/home")
	public HomeRepresentation getHomePage(){
		HomeActivity activity = new HomeActivity();
		return activity.getHomePage();
	}
}
