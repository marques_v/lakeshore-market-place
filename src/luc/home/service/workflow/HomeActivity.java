package luc.home.service.workflow;

import luc.home.service.representation.HomeRepresentation;
import luc.link.model.Link;

public class HomeActivity {
	
	public HomeActivity(){
		
	}
	
	public HomeRepresentation getHomePage(){
		
		HomeRepresentation rep = new HomeRepresentation();
		rep.setWelcome("Welcome to the marketplace");
		Link clientCreation = new Link("create client","http://localhost:8090/marketservice/client");
		Link partnerCreation = new Link("create partner","http://localhost:8090/partnerservice/partner/");
		Link partnerLogin = new Link("login partner","http://localhost:8090/loginservice/partner");
		Link clientLogin = new Link("login client","http://localhost:8090/loginservice/client");
		
		rep.setLinks(clientCreation,partnerCreation,partnerLogin,clientLogin);
		
		return rep;
	}
}
