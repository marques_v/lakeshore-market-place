package luc.home.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import luc.link.model.Link;
import service.AbstractRepresentation;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class HomeRepresentation extends AbstractRepresentation{

	//private Link loginClient;
	//private Link loginPartner;
	//private Link clientCreation;
	//private Link partnerCreation;
	private String welcome;
	
	public HomeRepresentation(){
		
	}
	
	public String getWelcome() {
		return welcome;
	}

	public void setWelcome(String welcome) {
		this.welcome = welcome;
	}
	
}
