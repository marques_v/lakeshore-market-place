package luc.login.business;

import java.io.IOException;
import java.sql.SQLException;

import luc.customer.dao.ClientDao;
import luc.customer.model.Client;
import luc.partner.dao.PartnerDao;
import luc.partner.model.Partner;

public class LoginManager {

	private PartnerDao pd;
	private ClientDao cd;
	private static Client loggedClient = null;
	private static Partner loggedPartner = null;
	
	public LoginManager(){
		
		try {
			this.pd = new PartnerDao();
			this.cd = new ClientDao();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Client loginAsClient(String userName, String userPassword) throws SQLException, IOException{
		
		Client c = cd.consult(userName);
		
		if(c != null){
			if(c.getUserPassword().equals(userPassword)){
				loggedClient = c;
				loggedPartner = null;
				return c;				
			}
		}
		
		return null;
	}
	
	public Partner loginAsPartner(String userName, String userPassword) throws SQLException{
		
		Partner p = (Partner) pd.consultByUserName(userName);
		
		if(p != null){
			if(p.getUserPassword().equals(userPassword)){
				loggedPartner = p;
				loggedClient = null;
				return p;
			}
		}
		
		return null;
	}

	public static Client getLoggedClient() {
		return loggedClient;
	}

	public static void setLoggedClient(Client loggedClient) {
		LoginManager.loggedClient = loggedClient;
	}

	public static Partner getLoggedPartner() {
		return loggedPartner;
	}

	public static void setLoggedPartner(Partner loggedPartner) {
		LoginManager.loggedPartner = loggedPartner;
	}
	
}
