package luc.login.service;

import luc.customer.service.representation.ClientRepresentation;
import luc.login.service.representation.LoginRequest;
import luc.partner.service.representation.PartnerRepresentation;

public interface LoginService {
	
	PartnerRepresentation loginAsPartner(LoginRequest req);
	ClientRepresentation loginAsClient(LoginRequest req);
	PartnerRepresentation getLoggedPartner();
	ClientRepresentation getLoggedClient();
}
