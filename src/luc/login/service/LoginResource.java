package luc.login.service;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import luc.customer.service.representation.ClientRepresentation;
import luc.login.service.representation.LoginRequest;
import luc.login.service.workflow.LoginActivity;
import luc.partner.service.representation.PartnerRepresentation;

@Path("/loginservice")
public class LoginResource implements LoginService{
	
	@Override
	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/partner")
	public PartnerRepresentation loginAsPartner(LoginRequest req) {
		
		LoginActivity activity = new LoginActivity();
		return activity.loginAsPartner(req);
		
	}
	
	@Override
	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/client")
	public ClientRepresentation loginAsClient(LoginRequest req) {
		
		LoginActivity activity = new LoginActivity();
		return activity.loginAsClient(req);
	}
	
	@Override
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/partner")
	public PartnerRepresentation getLoggedPartner(){
		
		LoginActivity activity = new LoginActivity();
		return activity.getLoggegPartner();
	}
	
	@Override
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/client")
	public ClientRepresentation getLoggedClient(){
		
		LoginActivity activity = new LoginActivity();
		return activity.getLoggegClient();
	}
	
}
