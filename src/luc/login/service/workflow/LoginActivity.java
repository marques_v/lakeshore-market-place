package luc.login.service.workflow;

import java.io.IOException;
import java.sql.SQLException;

import luc.address.model.Address;
import luc.address.service.representation.AddressRepresentation;
import luc.customer.model.Client;
import luc.customer.service.representation.ClientRepresentation;
import luc.link.model.Link;
import luc.login.business.LoginManager;
import luc.login.service.representation.LoginRequest;
import luc.partner.model.Partner;
import luc.partner.service.representation.PartnerRepresentation;

public class LoginActivity {

	private LoginManager manager;
	
	public LoginActivity(){
		this.manager = new LoginManager();
	}
	
	public ClientRepresentation loginAsClient(LoginRequest req){
		
		ClientRepresentation rep = new ClientRepresentation();
		try {
			
			Client c = manager.loginAsClient(req.getUserName(), req.getUserPassword());
			Address ad = c.getAddress();
			if(c != null){
				
				AddressRepresentation ar = new AddressRepresentation();
				ar.setAddressInfo1(ad.getAddressInfo1());
				ar.setAddressInfo2(ad.getAddressInfo2());
				ar.setCity(ad.getCity());
				ar.setState(ad.getState());
				ar.setCountry(ad.getCountry());
				ar.setIdAddress(ad.getIdAddress());
				
				rep.setAddress(ar);
				rep.setIdClient(c.getIdClient());
				rep.setMembershipDate(c.getMembershipDate());
				rep.setName(c.getName());
				rep.setUserName(c.getUserName());
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		setClientLinks(rep);
		return rep;
	}
	
	public PartnerRepresentation loginAsPartner(LoginRequest req){
		
		PartnerRepresentation rep = new PartnerRepresentation();
		try {
			
			Partner p = manager.loginAsPartner(req.getUserName(), req.getUserPassword());
			if(p != null){
				
				rep.setCompanyName(p.getCompanyName());
				rep.setUserName(p.getUserName());
				rep.setContractDate(p.getContractDate());
				rep.setIdPartner(p.getIdPartner());
				
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		setPartnerLinks(rep);
		return rep;
	}
	
	public PartnerRepresentation getLoggegPartner(){
		
		PartnerRepresentation rep = null;
		Partner p = LoginManager.getLoggedPartner();
		
		if(p != null){
			rep = new PartnerRepresentation();
			rep.setCompanyName(p.getCompanyName());
			rep.setContractDate(p.getContractDate());
			rep.setIdPartner(p.getIdPartner());
			rep.setUserName(p.getUserName());
		}
		
		return rep;
	}
	
public ClientRepresentation getLoggegClient(){
		
		ClientRepresentation rep = null;
		Client c = LoginManager.getLoggedClient();
		
		if(c != null){
			rep = new ClientRepresentation();
			rep.setIdClient(c.getIdClient());
			rep.setMembershipDate(c.getMembershipDate());
			rep.setName(c.getName());
			rep.setUserName(c.getUserName());
			
			AddressRepresentation ar = new AddressRepresentation();
			Address ad = c.getAddress();
			ar.setAddressInfo1(ad.getAddressInfo1());
			ar.setAddressInfo2(ad.getAddressInfo2());
			ar.setCity(ad.getCity());
			ar.setState(ad.getState());
			ar.setCountry(ad.getCountry());
			ar.setIdAddress(ad.getIdAddress());
			
			rep.setAddress(ar);
			
		}
		
		return rep;
	}
	
	public void setClientLinks(ClientRepresentation rep){
		
		if(rep.getUserName() == null){
			Link createClient = new Link("create","http://localhost:8090/marketservice/client");
			rep.setLinks(createClient);
		}else{
			Link delete = new Link("delete","http://localhost:8090/marketservice/client/"+	rep.getIdClient());
			Link edit = new Link("edit","http://localhost:8090/marketservice/client/"+ rep.getIdClient());
			Link searchProduct = new Link("search product","http://localhost:8090/productservice/search/product?name=");
			Link searchBuyOrders = new Link("search buyorders","http://localhost:8090/buyorderservice/buyorder/client/"+ 
					rep.getIdClient());
			rep.setLinks(delete,edit,searchProduct,searchBuyOrders);
		}
		
	}
	
	public void setPartnerLinks(PartnerRepresentation rep){
		
		if(rep.getUserName() == null){
			Link createPartner = new Link("create","http://localhost:8090/partnerservice/partner/");
			rep.setLinks(createPartner);
		}else{
			Link delete = new Link("delete","http://localhost:8090/partnerservice/partner/"+
					rep.getIdPartner());
			Link createProduct = new Link("create product","http://localhost:8090/productservice/product/");
			rep.setLinks(delete,createProduct);
		}
	}
}
