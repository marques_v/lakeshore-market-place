package luc.customer.service;

import java.util.Set;
import javax.ws.rs.core.Response;
import luc.customer.service.representation.*;

public interface ClientService {
	Set<ClientRepresentation> getClients();
	ClientRepresentation getClient(int id);
	ClientRepresentation createClient(ClientRequest c);
	Response deleteClient(int id);
	Response updateClient(int id, ClientRequest c);
}
