package luc.customer.service.representation;

import java.sql.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import luc.address.model.Address;
import luc.address.service.representation.AddressRepresentation;
import service.AbstractRepresentation;

@XmlRootElement(name = "Client")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class ClientRepresentation extends AbstractRepresentation{
	
	private Integer idClient;
	private String name;
	private Date membershipDate;
	private String userName;
	private AddressRepresentation address;
	
	
	public Integer getIdClient() {
		return idClient;
	}
	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getMembershipDate() {
		return membershipDate;
	}
	public void setMembershipDate(Date membershipDate) {
		this.membershipDate = membershipDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public AddressRepresentation getAddress() {
		return address;
	}
	public void setAddress(AddressRepresentation address) {
		this.address = address;
	}
	
	
}
