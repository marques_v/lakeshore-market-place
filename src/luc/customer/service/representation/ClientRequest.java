package luc.customer.service.representation;

import java.sql.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import luc.address.model.Address;
import luc.address.service.representation.AddressRequest;

@XmlRootElement(name = "Client")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class ClientRequest {
	
	private String name;
	private Date membershipDate;
	private String userName;
	private AddressRequest address;
	private String userPassword;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return userPassword;
	}
	public void setPassword(String password) {
		this.userPassword = password;
	}
	public Date getMembershipDate() {
		return membershipDate;
	}
	public void setMembershipDate(Date membershipDate) {
		this.membershipDate = membershipDate;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public AddressRequest getAddress() {
		return address;
	}
	public void setAddress(AddressRequest address) {
		this.address = address;
	}
	
	
}
