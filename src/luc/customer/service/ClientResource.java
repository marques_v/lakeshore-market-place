package luc.customer.service;

import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import luc.customer.service.representation.ClientRepresentation;
import luc.customer.service.representation.ClientRequest;
import luc.customer.service.workflow.ClientActivity;

@Path("/marketservice/")
public class ClientResource implements ClientService{
	
	public ClientResource(){}

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/client")
	public Set<ClientRepresentation> getClients() {
		System.out.println("GET METHOD Request for all Clients .............");
		ClientActivity a = new ClientActivity();
		return a.getClients();
	}

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/client/{clientId}")
	public ClientRepresentation getClient(@PathParam("clientId") int id) {
		ClientActivity a = new ClientActivity();
		return a.getClient(id);
		
	}

	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/client")
	public ClientRepresentation createClient(ClientRequest request) {
		ClientActivity a = new ClientActivity();
		return a.createClient(request);
	}

	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/client/{clientId}")
	public Response deleteClient(@PathParam("clientId") int id) {
		ClientActivity a = new ClientActivity();
		String res = a.deleteClient(id);
		if (res.equals("OK")) {
			return Response.status(Status.OK).build();
		}
		return null;
	}

	@PUT
	@Produces({"application/xml" , "application/json"})
	@Path("/client/{clientId}")
	public Response updateClient(@PathParam("clientId") int id, ClientRequest c) {
		ClientActivity a = new ClientActivity();
		String res = a.updateClient(id,c);
		if (res.equals("OK")) {
			return Response.status(Status.OK).build();
		}
		return null;
	}
	
}
