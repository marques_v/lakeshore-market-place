package luc.customer.service.workflow;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import luc.address.dao.AddressDao;
import luc.address.model.Address;
import luc.address.service.representation.AddressRepresentation;
import luc.address.service.representation.AddressRequest;
import luc.customer.business.CustomerRegistration;
import luc.customer.dao.ClientDao;
import luc.customer.model.Client;
import luc.customer.service.representation.ClientRepresentation;
import luc.customer.service.representation.ClientRequest;
import luc.link.model.Link;

public class ClientActivity {
	
	private static ClientDao cd;
	private static CustomerRegistration cr;
	
	public ClientActivity(){
		try {
			this.cd = new ClientDao();
			this.cr = new CustomerRegistration();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public Set<ClientRepresentation> getClients(){
		
		ArrayList<Client> l = new ArrayList();
		try {
			l = (ArrayList<Client>) cd.getAll();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Set<ClientRepresentation> s = new HashSet();
		
		Iterator it = l.iterator();
		while(it.hasNext()){
			Client c = (Client) it.next();
			ClientRepresentation rep = new ClientRepresentation();
			Address ad = c.getAddress();
			
			AddressRepresentation ar = new AddressRepresentation();
			ar.setAddressInfo1(ad.getAddressInfo1());
			ar.setAddressInfo2(ad.getAddressInfo2());
			ar.setCity(ad.getCity());
			ar.setState(ad.getState());
			ar.setCountry(ad.getCountry());
			ar.setIdAddress(ad.getIdAddress());
			
			rep.setAddress(ar);
			rep.setIdClient(c.getIdClient());
			rep.setMembershipDate(c.getMembershipDate());
			rep.setName(c.getName());
			rep.setUserName(c.getUserName());
			s.add(rep);
		}
		
		return s;
	}
	
	public ClientRepresentation getClient(int id){
		
		Client c = new Client();
		try {
			c = cd.consult(new Integer(id));
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		
		ClientRepresentation rep = new ClientRepresentation();
		
		Address ad = c.getAddress();
		
		AddressRepresentation ar = new AddressRepresentation();
		ar.setAddressInfo1(ad.getAddressInfo1());
		ar.setAddressInfo2(ad.getAddressInfo2());
		ar.setCity(ad.getCity());
		ar.setState(ad.getState());
		ar.setCountry(ad.getCountry());
		ar.setIdAddress(ad.getIdAddress());
		
		rep.setAddress(ar);
		rep.setIdClient(c.getIdClient());
		rep.setMembershipDate(c.getMembershipDate());
		rep.setName(c.getName());
		rep.setUserName(c.getUserName());
		
		setLinks(rep);
		
		return rep;
	}
	
	public ClientRepresentation createClient(ClientRequest request){
		
		ClientRepresentation rep = null;
		
		AddressRequest aReq = request.getAddress();
		Address ad = new Address(aReq.getAddressInfo1(),aReq.getAddressInfo2(),aReq.getCity(),
				aReq.getState(),aReq.getCountry());
				
		try {
			if(cr.insert(request.getName(),request.getMembershipDate(),request.getUserName(),
					request.getPassword(),ad)){
				
				AddressRepresentation aRep = new AddressRepresentation();
				aRep.setAddressInfo1(ad.getAddressInfo1());
				aRep.setAddressInfo2(ad.getAddressInfo2());
				aRep.setCity(ad.getCity());
				aRep.setState(ad.getState());
				aRep.setCountry(ad.getCountry());
				
				AddressDao addressDao = new AddressDao();
				Address address = addressDao.consult(ad.getAddressInfo1());
				aRep.setIdAddress(address.getIdAddress().intValue());
				
				rep = new ClientRepresentation();
				rep.setAddress(aRep);
				rep.setMembershipDate(request.getMembershipDate());
				rep.setName(request.getName());
				rep.setUserName(request.getUserName());
				rep.setIdClient(cd.consult(request.getUserName()).getIdClient());
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
		
		return rep;
	}
	
	public String deleteClient(int id){
		
		Client c = null;
		String response = "NOT OK";
		
		try {
			c = cd.consult(new Integer(id));
			if(cr.remove(c.getUserName()))
				return "OK";
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		}
			
		return response;
	}
	
	public String updateClient(int id, ClientRequest request){
		
		
		String response = "NOT OK";
		
		AddressRequest ar = request.getAddress();
		Address address = new Address(ar.getAddressInfo1(),ar.getAddressInfo2(),ar.getCity(),
				ar.getState(),ar.getCountry());
		Client c = new Client(request.getName(),request.getMembershipDate(),request.getUserName(),request.getPassword(),
				address);
		try {		
			if(cr.edit(new Integer(id),c)){
				return "OK";
			}
		} catch (SQLException | IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	public void setLinks(ClientRepresentation rep){
		
		Link edit = new Link("edit","http://localhost:8090/marketservice/client");
		
		Link delete = new Link("delete","http://localhost:8090/marketservice/client/"+
		rep.getIdClient());
		
		rep.setLinks(edit,delete);
	}
}
