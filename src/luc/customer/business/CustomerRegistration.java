package luc.customer.business;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

import luc.address.dao.AddressDao;
import luc.address.model.Address;
import luc.customer.builder.ClientBuilder;
import luc.customer.builder.ClientDirector;
import luc.customer.dao.ClientDao;
import luc.customer.model.Client;

public class CustomerRegistration {
	private ClientDirector cd;
	private ClientDao clientDao;
	private Client client;
	
	public CustomerRegistration() throws IOException{
		 cd = new ClientDirector(new ClientBuilder());
		 clientDao = new ClientDao();
	}
	
	public boolean insert(String name, Date date, String userName, String userPassword, 
			Address address) throws SQLException, IOException{
		
		boolean retorno = false;
		AddressDao aDao = new AddressDao();
		Address ad;
		
		if(clientDao.consult(userName)==null){
			
			if(name.equals(null))
				name = "empty";
			if(userPassword.equals(null))
				userPassword = "empty";
			
			if(address.getIdAddress() == null){
				aDao.insert(address);
				ad = aDao.consult(address.getAddressInfo1());
			}else if(aDao.consult(new Integer(address.getIdAddress())) == null){
				aDao.insert(address);
				ad = aDao.consult(address.getAddressInfo1());
			}else{
				ad = address;
			}
			
			client = cd.buildClient(name, date, userName, userPassword, ad);
			clientDao.insert(client);
			retorno = true;
		}
		else{
			System.out.println("Client already exists");
		}
		return retorno;
	}
	//The empty fields in the business layer you should fill it as empty
	public boolean edit(Integer id, Client client) throws Exception{
		Client c = clientDao.consult(id);
		boolean retorno = false;
		if(c!=null){
			if(!client.getUserName().equals(c.getUserName())){
				throw new Exception("Changing the username is not allowed");
			}
			if(client.getName()=="empty"){
				client.setName(c.getName());
			}
			if(client.getUserPassword()=="empty"){
				client.setUserPassword(c.getUserPassword());
			}
			if(client.getAddress()==null){
				client.setAddress(c.getAddress());
			}
			clientDao.edit(c, client.getName(), client.getMembershipDate(), 
						   c.getUserName(), client.getUserPassword(), client.getAddress());
			retorno = true;
		}
		else
			clientDao.insert(client);
		return retorno;
		
	}
	
	public boolean remove(String userName) throws SQLException, IOException{
		boolean retorno = false;
		if(clientDao.consult(userName)!=null){
			clientDao.remove(userName);
			retorno = true;
		}
		
		return retorno;
	}
	

	
	
	
}
