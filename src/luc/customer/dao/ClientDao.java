package luc.customer.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Date;
import java.sql.PreparedStatement;

import luc.address.dao.AddressDao;
import luc.address.model.Address;
import luc.customer.builder.ClientBuilder;
import luc.customer.builder.ClientDirector;
import luc.customer.model.Client;
import luc.persistence.DataBase;

public class ClientDao{

	private DataBase db;
	private PreparedStatement ps;
	private ClientDirector director;

	public ClientDao() throws IOException {
		//this.clients = new ArrayList();
		db = new DataBase();
		director = new ClientDirector(new ClientBuilder());
	}
	
	public Client consult(Object field) throws SQLException, IOException{
		Client c = null;
		Connection con = db.openConnection();
		
		if(field instanceof String){
			String userName = field.toString();
			String query = "SELECT * FROM client WHERE user_name = ?;";
			ps = con.prepareStatement(query);
			ps.setString(1, userName);
			ResultSet rs = ps.executeQuery();
			
			AddressDao ad = new AddressDao();
			if(rs.next()){
				Address address = ad.consult(new Integer(rs.getInt("Address_idAddress")));
				c = director.buildClient(rs.getString("name"),rs.getDate("membership_date"),rs.getString("user_name"),rs.getString("user_password"),address);
				c.setIdClient(rs.getInt("idClient"));
			}
		}else if(field instanceof Integer){
			int id = ((Integer) field).intValue();
			String query = "SELECT * FROM client WHERE idClient = ?;";
			ps = con.prepareStatement(query);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			AddressDao ad = new AddressDao();
			if(rs.next()){
				Address address = ad.consult(new Integer(rs.getInt("Address_idAddress")));
				c = director.buildClient(rs.getString("name"),rs.getDate("membership_date"),rs.getString("user_name"),rs.getString("user_password"),address);
				c.setIdClient(rs.getInt("idClient"));
			}
		}
		
		db.closeConnection();
		return c;
	}
	
	public boolean insert(Client c) throws SQLException, IOException{
		
		String query = "INSERT INTO client (name,membership_date,user_name,user_password, Address_idAddress) VALUES(?,?,?,?,?);";
		Connection con = db.openConnection();
		ps = con.prepareStatement(query);
		ps.setString(1, c.getName());
		ps.setDate(2, c.getMembershipDate());
		ps.setString(3, c.getUserName());
		ps.setString(4, c.getUserPassword());
		ps.setInt(5,c.getAddress().getIdAddress());
		ps.execute();
		db.closeConnection();
		if(consult(c.getUserName()).equals(null)){
			return false;
		}else{
			return true;
		}	
	}
	
	public List getAll() throws SQLException, IOException{
		
		String query = "SELECT * FROM client;";
		
		Connection con = db.openConnection();
		ps = con.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		ArrayList result = new ArrayList();
		AddressDao ad = new AddressDao();
		
		while(rs.next()){
			Client c = new Client();
			c.setIdClient(rs.getInt("idClient"));
			c.setName(rs.getString("name"));
			c.setMembershipDate(rs.getDate("membership_date"));
			c.setUserName(rs.getString("user_name"));
			c.setUserPassword(rs.getString("user_password"));
			c.setAddress(ad.consult(new Integer(rs.getInt("Address_idAddress"))));
			result.add(c);
		}
		db.closeConnection();
		
		return result;
	}
	
	public boolean remove(Object field) throws SQLException, IOException{
		
		Connection con = db.openConnection();
		
		if(field instanceof Integer){
			int id = ((Integer) field).intValue();
			if(consult(id).equals(null)){
				return false;
			}else{
				String query = "DELETE FROM client WHERE idClient = ?;";
				ps = con.clientPrepareStatement(query);
				ps.setInt(1, id);
				ps.execute();		
				db.closeConnection();
				return true;
			}
		}else if(field instanceof String){
			String userName = field.toString();
			if(consult(userName).equals(null)){
				return false;
			}else{
				String query = "DELETE FROM client WHERE user_name = ?;";
				ps = con.prepareStatement(query);
				ps.setString(1, userName);
				ps.execute();		
				db.closeConnection();
				return true;
			}
		}
		
		
		return false;
	}
	
	
	public boolean edit(Client c, String name2, Date membershipDate2, String userName2, String password2, Address address) throws SQLException, IOException{
		
		boolean flag = false;
		String query = "UPDATE client SET client.name = ?, membership_date = ?, user_name = ?, user_password = ?, Address_idAddress = ? WHERE idClient = ?;";
		
		if(!consult(c.getIdClient()).equals(null)){	
			Connection con = db.openConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, name2);
			ps.setDate(2, membershipDate2);
			ps.setString(3, userName2);
			ps.setString(4, password2);
			ps.setInt(5, address.getIdAddress());
			ps.setInt(6, c.getIdClient());
			ps.executeUpdate();
			flag = true;	
			db.closeConnection();
		}
		
		return flag;	
	}

}
