package luc.customer.model;

import java.sql.Date;

import javax.xml.bind.annotation.XmlRootElement;

import luc.address.model.Address;

@XmlRootElement
public class Client {

	private Integer idClient;
	private String name;
	private Date membershipDate;
	private String userName;
	private String userPassword;
	private Address address;

	public Client() {
	}

	
	public Client(String name, Date membershipDate, String userName, String userPassword, Address address) {
		this.idClient = null;
		this.name = name;
		this.membershipDate = membershipDate;
		this.userName = userName;
		this.userPassword = userPassword;
		this.address = address;
	}
	

	public Integer getIdClient() {
		return this.idClient;
	}

	public void setIdClient(Integer idClient) {
		this.idClient = idClient;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getMembershipDate() {
		return this.membershipDate;
	}

	public void setMembershipDate(Date membershipDate) {
		this.membershipDate = membershipDate;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	public Address getAddress(){
		return this.address;
	}
	
	public void setAddress(Address address){
		this.address = address;
	}

}
