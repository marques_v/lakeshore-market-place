package luc.customer.builder;

import java.sql.Date;

import luc.address.model.Address;
import luc.customer.model.Client;

public class ClientDirector {

	private AbstractClientBuilder builder;
	
	public ClientDirector(AbstractClientBuilder builder){
		this.builder = builder;
	}
	
	public Client buildClient(String name, Date date, String userName, String userPassword, 
			Address address){
				
		builder.createClient();
		builder.addName(name);
		builder.addMembershipDate(date);
		builder.addUsername(userName);
		builder.addUserPassword(userPassword);
		builder.addAddress(address);
		return builder.getClient();		
	}
}
