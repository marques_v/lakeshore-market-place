package luc.customer.builder;

import java.sql.Date;

import luc.address.model.Address;
import luc.customer.model.Client;

public abstract class AbstractClientBuilder {

	protected Client client;
	
	public abstract void createClient();
	
	public abstract void addAddress(Address address);
	
	public abstract void addName(String name);
	
	public abstract void addMembershipDate(Date date);
	
	public abstract void addUsername(String userName);
	
	public abstract void addUserPassword(String userPassword);
	
	public Client getClient(){
		return this.client;
	}
}
