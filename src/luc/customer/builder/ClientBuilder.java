package luc.customer.builder;

import java.sql.Date;

import luc.address.model.Address;
import luc.customer.model.Client;

public class ClientBuilder extends AbstractClientBuilder{

	@Override
	public void createClient() {
		this.client = new Client();	
	}

	@Override
	public void addAddress(Address address){
		client.setAddress(address);
		
	}

	@Override
	public void addName(String name) {
		client.setName(name);
		
	}

	@Override
	public void addMembershipDate(Date date) {
		client.setMembershipDate(date);
		
	}

	@Override
	public void addUsername(String userName) {
		client.setUserName(userName);
		
	}

	@Override
	public void addUserPassword(String userPassword) {
		client.setUserPassword(userPassword);
		
	}

}
