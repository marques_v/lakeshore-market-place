package luc.order.service;

import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import luc.order.service.representation.BuyOrderRepresentation;
import luc.order.service.representation.BuyOrderRequest;
import luc.order.service.workflow.BuyOrderActivity;

@Path("/buyorderservice/")
public class BuyOrderResource implements BuyOrderService{
	
	private BuyOrderActivity buyorderActivity;
	
	public BuyOrderResource(){
		buyorderActivity = new BuyOrderActivity();
	}

	@Override
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/buyorder")
	public Set<BuyOrderRepresentation> getBuyOrders() {
		System.out.println("GET METHOD Request for all the Buy Orders .............");
		return buyorderActivity.getBuyOrders();
	}

	@Override
	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/buyorder")
	public BuyOrderRepresentation buy(BuyOrderRequest buyorderRequest) {
		System.out.println("POST METHOD Request for a Buy Order creation .............");
		return buyorderActivity.buy(buyorderRequest);
	}

	@Override
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/buyorder/{idBuyOrder}/payment")
	public Response pay(@PathParam("idBuyOrder") Integer id) {
		System.out.println("GET METHOD Request for a Buy Order payment .............");
		if( buyorderActivity.pay(id) ){
			return Response.status(Status.OK).build();
		}
		return null;
	}

	@Override
	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/buyorder/{idBuyOrder}")
	public Response calcelBuyOrder(@PathParam("idBuyOrder") Integer id) {
		System.out.println("DELETE METHOD Request for a Buy Order cancelling .............");
		if( buyorderActivity.cancel(id) ){
			return Response.status(Status.OK).build();
		}
		return null;
	}

	@Override
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/buyorder/{idBuyOrder}")
	public BuyOrderRepresentation getBuyOrder(@PathParam("idBuyOrder") Integer id) {
		System.out.println("GET METHOD Request for a particular Buy Order .............");
		return buyorderActivity.getBuyOrder(id);
	}
	
	@Override
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/buyorder/client/{idClient}")
	public Set<BuyOrderRepresentation> getClientBuyOrders(@PathParam("idClient") int id) {
		return buyorderActivity.getClientBuyOrders(id);
	}

	@Override
	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/buyorder/{idBuyOrder}/delayed")
	public Response setStatusToDelayed(@PathParam("idBuyOrder") Integer id) {
		System.out.println("POST METHOD Request for a particular Buy Order .............");
		if( buyorderActivity.changeStatusToDelayed(id) ){
			return Response.status(Status.OK).build();
		}
		return null;
	}

}
