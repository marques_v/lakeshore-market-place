package luc.order.service;

import java.util.Set;

import javax.jws.WebService;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

import luc.order.service.representation.BuyOrderRepresentation;
import luc.order.service.representation.BuyOrderRequest;

@WebService
public interface BuyOrderService {
	public Set<BuyOrderRepresentation> getBuyOrders();
	public BuyOrderRepresentation getBuyOrder(Integer id);
	public Response setStatusToDelayed(Integer id);
	public Response pay(Integer id);
	public Response calcelBuyOrder(Integer id);
	public BuyOrderRepresentation buy(BuyOrderRequest buyorderRequest);
	Set<BuyOrderRepresentation> getClientBuyOrders(int id);
}
