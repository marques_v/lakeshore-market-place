package luc.order.service.workflow;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.core.Response;

import java.util.Iterator;

import luc.customer.model.Client;
import luc.link.model.Link;
import luc.login.business.LoginManager;
import luc.order.business.BuyOrderManager;
import luc.order.business.Status;
import luc.order.service.representation.BuyOrderRepresentation;
import luc.order.service.representation.BuyOrderRequest;
import luc.order.model.BuyOrder;

public class BuyOrderActivity {
	private static BuyOrderManager buymanager;
	private LoginManager lm;
	
	public BuyOrderActivity(){
		buymanager = new BuyOrderManager();
		
		
	}
	
	public Set<BuyOrderRepresentation> getClientBuyOrders(int id){
		
		ArrayList<BuyOrder> buyOrders = buymanager.getClientBuyOrders(id);
		Set<BuyOrderRepresentation> buyOrderRepresentations = new HashSet<BuyOrderRepresentation>();
		
		Iterator i = buyOrders.iterator();
		while(i.hasNext()){
			BuyOrder bo = (BuyOrder) i.next();
			BuyOrderRepresentation buyorderRepre = new BuyOrderRepresentation();
			buyorderRepre.setDate(bo.getDate());
			buyorderRepre.setIdBuyOrder(bo.getIdBuyOrder());
			buyorderRepre.setPaymentNumberFixed(bo.getPaymentNumberFixed());
			buyorderRepre.setPaymentNumberFulfilled(bo.getPaymentNumberFulfilled());
			buyorderRepre.setTotal(bo.getTotal());
			buyorderRepre.setStatus(bo.getStatus().toString());
			buyorderRepre.setClientIdClient(bo.getClientIdClient());
			
			buyOrderRepresentations.add(buyorderRepre);
		}
		
		return buyOrderRepresentations;	
	}

	public Set<BuyOrderRepresentation> getBuyOrders() {
		ArrayList<BuyOrder> buyOrders = buymanager.getAllBuyOrder();
		Set<BuyOrderRepresentation> buyOrderRepresentations = new HashSet<BuyOrderRepresentation>();
		
		Iterator i =  buyOrders.iterator();
		while(i.hasNext()){
			BuyOrder bo = (BuyOrder) i.next();
			BuyOrderRepresentation buyorderRepre = new BuyOrderRepresentation();
			buyorderRepre.setDate(bo.getDate());
			buyorderRepre.setIdBuyOrder(bo.getIdBuyOrder());
			buyorderRepre.setPaymentNumberFixed(bo.getPaymentNumberFixed());
			buyorderRepre.setPaymentNumberFulfilled(bo.getPaymentNumberFulfilled());
			buyorderRepre.setTotal(bo.getTotal());
			buyorderRepre.setStatus(bo.getStatus().toString());
			buyorderRepre.setClientIdClient(bo.getClientIdClient());
			
			buyOrderRepresentations.add(buyorderRepre);
		}
		
		return buyOrderRepresentations;
	}

	public BuyOrderRepresentation buy(BuyOrderRequest buyReq) {
		BuyOrder bo = buymanager.buy(buyReq.getProductId(), buyReq.getQuantity(), buyReq.getClientIdClient(), buyReq.getTotal(), 
				buyReq.getPaymentNumberFixed(), buyReq.getEstimatedArrival());
		BuyOrderRepresentation boR = new BuyOrderRepresentation();
		boR.setDate(bo.getDate());
		boR.setIdBuyOrder(bo.getIdBuyOrder());
		boR.setPaymentNumberFixed(bo.getPaymentNumberFixed());
		boR.setPaymentNumberFulfilled(bo.getPaymentNumberFulfilled());
		boR.setTotal(bo.getTotal());
		boR.setStatus(bo.getStatus().toString());
		boR.setClientIdClient(bo.getClientIdClient());
		
		setLinksBuy(boR);
		
		return boR;
	}

	public boolean pay(Integer id) {
		
		boolean flag = buymanager.payment(id);
		if(flag){
			BuyOrderRepresentation rep = new BuyOrderRepresentation();
			rep.setIdBuyOrder(id);
		}
		
		return flag;		
	}

	public boolean cancel(Integer id) {
		return buymanager.changeStatus(id, Status.CANCELED);
	}

	public BuyOrderRepresentation getBuyOrder(Integer id) {
		BuyOrder bo;
		BuyOrderRepresentation buyorderRepre = new BuyOrderRepresentation();
		
		bo = buymanager.retriveve(id);
		buyorderRepre.setDate(bo.getDate());
		buyorderRepre.setIdBuyOrder(bo.getIdBuyOrder());
		buyorderRepre.setPaymentNumberFixed(bo.getPaymentNumberFixed());
		buyorderRepre.setPaymentNumberFulfilled(bo.getPaymentNumberFulfilled());
		buyorderRepre.setTotal(bo.getTotal());
		buyorderRepre.setStatus(bo.getStatus().toString());
		buyorderRepre.setClientIdClient(bo.getClientIdClient());
		
		setLinksGet(buyorderRepre);
		
		return buyorderRepre;
	}

	public boolean changeStatusToDelayed(Integer id) {
		return buymanager.changeStatus(id, Status.DELAYED);
	}
	
	public void setLinksGet(BuyOrderRepresentation rep){
		
		LoginManager lm = new LoginManager();
		Client loggedClient = lm.getLoggedClient();
		
		if(loggedClient != null){
			if(loggedClient.getIdClient() == rep.getClientIdClient()){
				
				Link cancel = new Link("cancel","http://localhost:8090/buyorderservice/buyorder/"+
				rep.getIdBuyOrder());
				
				Link pay = new Link("pay","http://localhost:8090/buyorderservice/buyorder/"+
				rep.getIdBuyOrder()+"/payment");
				
				rep.setLinks(cancel,pay);
			}
		}
	}
	
	public void setLinksBuy(BuyOrderRepresentation rep){
		
		LoginManager lm = new LoginManager();
		Client loggedClient = lm.getLoggedClient();
		
		if(loggedClient != null){
			if(loggedClient.getIdClient() == rep.getClientIdClient()){
				
				Link writeReview = new Link("write review","http://localhost:8090/marketservice/review");
				rep.setLinks(writeReview);
			}
		}
	}
	
}
