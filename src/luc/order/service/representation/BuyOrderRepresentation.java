package luc.order.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import service.AbstractRepresentation;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class BuyOrderRepresentation extends AbstractRepresentation{
	private Integer idBuyOrder;
	private String date;
	private String status;
	private Float total;
	private Integer paymentNumberFixed;
	private Integer paymentNumberFulfilled;
	private int clientIdClient;
	
	
	public int getClientIdClient() {
		return clientIdClient;
	}
	public void setClientIdClient(int clientIdClient) {
		this.clientIdClient = clientIdClient;
	}
	public Integer getIdBuyOrder() {
		return idBuyOrder;
	}
	public void setIdBuyOrder(Integer idBuyOrder) {
		this.idBuyOrder = idBuyOrder;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Float getTotal() {
		return total;
	}
	public void setTotal(Float total) {
		this.total = total;
	}
	public Integer getPaymentNumberFixed() {
		return paymentNumberFixed;
	}
	public void setPaymentNumberFixed(Integer paymentNumberFixed) {
		this.paymentNumberFixed = paymentNumberFixed;
	}
	public Integer getPaymentNumberFulfilled() {
		return paymentNumberFulfilled;
	}
	public void setPaymentNumberFulfilled(Integer paymentNumberFulfilled) {
		this.paymentNumberFulfilled = paymentNumberFulfilled;
	}
	
	
}
