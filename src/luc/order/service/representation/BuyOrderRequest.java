package luc.order.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "BuyOrder")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class BuyOrderRequest {
	
	private Float total;
	private Integer paymentNumberFixed;
	private Integer paymentNumberFulfilled;
	private int clientIdClient;
	private int productId;
	private int quantity;
	private String estimatedArrival;
	
	
	public String getEstimatedArrival() {
		return estimatedArrival;
	}
	public void setEstimatedArrival(String estimatedArrival) {
		this.estimatedArrival = estimatedArrival;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Float getTotal() {
		return total;
	}
	public void setTotal(Float total) {
		this.total = total;
	}
	public Integer getPaymentNumberFixed() {
		return paymentNumberFixed;
	}
	public void setPaymentNumberFixed(Integer paymentNumberFixed) {
		this.paymentNumberFixed = paymentNumberFixed;
	}
	public Integer getPaymentNumberFulfilled() {
		return paymentNumberFulfilled;
	}
	public void setPaymentNumberFulfilled(Integer paymentNumberFulfilled) {
		this.paymentNumberFulfilled = paymentNumberFulfilled;
	}
	public int getClientIdClient() {
		return clientIdClient;
	}
	public void setClientIdClient(int clientIdClient) {
		this.clientIdClient = clientIdClient;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	
	
		
}
