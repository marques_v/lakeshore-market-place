package luc.order.model;

public class Payment {

	private Integer idPayment;
	private String date;
	private Float value;
	private int buyOrderIdBuyOrder;

	public Payment() {
	}

	public Payment(String date, Float value, int buyOrderIdBuyOrder) {
		this.date = date;
		this.value = value;
		this.buyOrderIdBuyOrder = buyOrderIdBuyOrder;
	}

	public Integer getIdPayment() {
		return this.idPayment;
	}

	public void setIdPayment(Integer idPayment) {
		this.idPayment = idPayment;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Float getValue() {
		return this.value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public int getBuyOrderIdBuyOrder() {
		return this.buyOrderIdBuyOrder;
	}

	public void setBuyOrderIdBuyOrder(int buyOrderIdBuyOrder) {
		this.buyOrderIdBuyOrder = buyOrderIdBuyOrder;
	}

}
