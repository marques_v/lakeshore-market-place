package luc.order.model;

import luc.order.business.Status;

public class ShipOrder{

	private Integer idShipOrder;
	private String date;
	private String estimatedArrival;
	private String effectiveArrival;
	private Status status;

	public ShipOrder() {
	}

	public ShipOrder(String date, String estimatedArrival, String effectiveArrival, Status status) {
		this.date = date;
		this.estimatedArrival = estimatedArrival;
		this.effectiveArrival = effectiveArrival;
		this.status = status;
	}

	public Integer getIdShipOrder() {
		return this.idShipOrder;
	}

	public void setIdShipOrder(Integer idShipOrder) {
		this.idShipOrder = idShipOrder;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getEstimatedArrival() {
		return this.estimatedArrival;
	}

	public void setEstimatedArrival(String estimatedArrival) {
		this.estimatedArrival = estimatedArrival;
	}

	public String getEffectiveArrival() {
		return this.effectiveArrival;
	}

	public void setEffectiveArrival(String effectiveArrival) {
		this.effectiveArrival = effectiveArrival;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

}
