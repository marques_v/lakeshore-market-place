package luc.order.model;

import luc.order.business.Status;

public class BuyOrder {

	private Integer idBuyOrder;
	private String date;
	private Status status;
	private Float total;
	private Integer paymentNumberFixed;
	private Integer paymentNumberFulfilled;
	private int clientIdClient;
	private int shipOrderIdShipOrder;

	public BuyOrder() {
	}

	public BuyOrder(int clientIdClient, int shipOrderIdShipOrder) {
		this.clientIdClient = clientIdClient;
		this.shipOrderIdShipOrder = shipOrderIdShipOrder;
	}

	public BuyOrder(String date, Status status, Float total, Integer paymentNumberFixed, Integer paymentNumberFulfilled,
			int clientIdClient, int shipOrderIdShipOrder) {
		this.date = date;
		this.status = status;
		this.total = total;
		this.paymentNumberFixed = paymentNumberFixed;
		this.paymentNumberFulfilled = paymentNumberFulfilled;
		this.clientIdClient = clientIdClient;
		this.shipOrderIdShipOrder = shipOrderIdShipOrder;
	}

	public Integer getIdBuyOrder() {
		return this.idBuyOrder;
	}

	public void setIdBuyOrder(Integer idBuyOrder) {
		this.idBuyOrder = idBuyOrder;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Status getStatus() {
		return this.status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Float getTotal() {
		return this.total;
	}

	public void setTotal(Float total) {
		this.total = total;
	}

	public Integer getPaymentNumberFixed() {
		return this.paymentNumberFixed;
	}

	public void setPaymentNumberFixed(Integer paymentNumberFixed) {
		this.paymentNumberFixed = paymentNumberFixed;
	}

	public Integer getPaymentNumberFulfilled() {
		return this.paymentNumberFulfilled;
	}

	public void setPaymentNumberFulfilled(Integer paymentNumberFulfilled) {
		this.paymentNumberFulfilled = paymentNumberFulfilled;
	}

	public int getClientIdClient() {
		return this.clientIdClient;
	}

	public void setClientIdClient(int clientIdClient) {
		this.clientIdClient = clientIdClient;
	}

	public int getShipOrderIdShipOrder() {
		return this.shipOrderIdShipOrder;
	}

	public void setShipOrderIdShipOrder(int shipOrderIdShipOrder) {
		this.shipOrderIdShipOrder = shipOrderIdShipOrder;
	}

}
