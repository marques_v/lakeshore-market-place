package luc.order.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import luc.order.business.Status;
import luc.order.model.ShipOrder;
import luc.persistence.Dao;
import luc.persistence.DataBase;

public class ShipOrderDao implements Dao{
	
	private Connection connection;
	
	public ShipOrderDao() throws IOException{
		DataBase db = new DataBase();
		connection = db.openConnection();
	}
	

	@Override
	public boolean insert(Object o) throws SQLException {
		String sql = "insert into ShipOrder (date, estimated_arrival, effective_arrival, status) values (?,?,?,?);";
        
		if(!o.getClass().equals(ShipOrder.class)){
			return false;
		}
		ShipOrder ship_order = (ShipOrder) o;
		
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ps.setString(1, ship_order.getDate());
		ps.setString(2, ship_order.getEstimatedArrival());
		ps.setString(3, ship_order.getEffectiveArrival());
		ps.setString(4, ship_order.getStatus().toString());
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public boolean edit(Object o) throws SQLException {
		String sql = "update ShipOrder set date=?,estimated_arrival=?,effective_arrival=?,status=? where idShipOrder=?;";
		
		if(!o.getClass().equals(ShipOrder.class)){
			return false;
		}
		
		ShipOrder ship_order = (ShipOrder) o;
		
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ps.setString(1, ship_order.getDate());
		ps.setString(2, ship_order.getEstimatedArrival());
		ps.setString(3, ship_order.getEffectiveArrival());
		ps.setString(4, ship_order.getStatus().toString());
		ps.setInt(5, ship_order.getIdShipOrder());
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public boolean remove(Object o) throws SQLException {
		Integer id = (Integer) o;
		
		String sql = "delete from ShipOrder where idShipOrder=? limit 1;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setInt(1, id);
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public Object consult(Object o) throws SQLException {
		Integer id = (Integer) o;
		
		String sql = "select * from ShipOrder where idShipOrder=?;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setInt(1, id);
		
		if(!ps.execute()){
			return null;
		}
		
		ResultSet rs = ps.executeQuery();
		rs.next();
		
		ShipOrder ship_order = new ShipOrder();
		ship_order.setDate(rs.getString("date"));
		ship_order.setEffectiveArrival(rs.getString("effective_arrival"));
		ship_order.setEstimatedArrival(rs.getString("estimated_arrival"));
		ship_order.setIdShipOrder(rs.getInt("idShipOrder"));
		ship_order.setStatus( Status.getStatus(rs.getString("status")));
		
		return ship_order;
	}

	@Override
	public List getAll() throws SQLException {
		String sql = "select * from ShipOrder;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		if(!ps.execute()){
			return null;
		}
		
		ResultSet rs = ps.executeQuery();
		
		ArrayList<ShipOrder> ship_orders = new ArrayList<ShipOrder>();
		while(rs.next()){
			ShipOrder so = new ShipOrder();
			so.setIdShipOrder(rs.getInt("idShipOrder"));
			so.setDate(rs.getString("date"));
			so.setEffectiveArrival(rs.getString("effective_arrival"));
			so.setEstimatedArrival(rs.getString("estimated_arrival"));
			so.setStatus( Status.getStatus(rs.getString("status")));
			
			ship_orders.add(so);
		}
		
		return ship_orders;
	}

	/*
	 * This method should be use to retrieve last row inserted on table
	 */
	public int getLastId() throws SQLException{
		String sql = "select idShipOrder from shiporder order by idShipOrder desc limit 1;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			return rs.getInt("idShipOrder");
		}
		
		return 0;
	}
}
