package luc.order.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import luc.order.business.Status;
import luc.order.model.BuyOrder;
import luc.order.model.ShipOrder;
import luc.persistence.Dao;
import luc.persistence.DataBase;

public class BuyOrderDao implements Dao{
	
	private Connection connection;
	
	//Opening the connection with the database
	public BuyOrderDao() throws IOException{
		DataBase db = new DataBase();
		connection = db.openConnection();
	}

	@Override
	public boolean insert(Object o) throws SQLException {
		
		String sql = "insert into BuyOrder (date,status,total,payment_number_fixed,payment_number_fulfilled,client_idClient,shipOrder_IdShipOrder) values (?,?,?,?,?,?,?);";
		if(!o.getClass().equals(BuyOrder.class)){
			return false;
		}
		
		BuyOrder buy_order = (BuyOrder) o;
		
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ps.setString(1, buy_order.getDate());
		ps.setString(2, buy_order.getStatus().toString());
		ps.setFloat(3, buy_order.getTotal());
		ps.setInt(4, buy_order.getPaymentNumberFixed());
		ps.setInt(5, buy_order.getPaymentNumberFulfilled());
		ps.setInt(6, buy_order.getClientIdClient());
		ps.setInt(7, buy_order.getShipOrderIdShipOrder());

		return ps.executeUpdate()!=0;
	}

	@Override
	public boolean edit(Object o) throws SQLException {
		String sql = "update BuyOrder set date=?,status=?,total=?,payment_number_fixed=?,payment_number_fulfilled=?,client_idClient=?, shipOrder_IdShipOrder=? where idBuyOrder=?";
		
		if(!o.getClass().equals(BuyOrder.class)){
			return false;
		}
		
		BuyOrder buy_order = (BuyOrder) o;
		
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ps.setString(1, buy_order.getDate());
		ps.setString(2, buy_order.getStatus().toString());
		ps.setFloat(3, buy_order.getTotal());
		ps.setInt(4, buy_order.getPaymentNumberFixed());
		ps.setInt(5, buy_order.getPaymentNumberFulfilled());
		ps.setInt(6, buy_order.getClientIdClient());
		ps.setInt(7, buy_order.getShipOrderIdShipOrder());
		ps.setInt(8, buy_order.getIdBuyOrder());
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public boolean remove(Object o) throws SQLException {
		Integer id = (Integer) o;
		
		String sql = "delete from BuyOrder where idBuyOrder=? limit 1;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setInt(1, id);
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public Object consult(Object o) throws SQLException {
		Integer id = (Integer) o;
		
		String sql = "select * from BuyOrder where idBuyOrder=?;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setInt(1, id);
		
		if(!ps.execute()){
			return null;
		}

		ResultSet rs = ps.executeQuery();
		rs.next();
		
		BuyOrder buy_order = new BuyOrder();
		buy_order.setIdBuyOrder(rs.getInt("idBuyOrder"));
		buy_order.setDate(rs.getString("date"));
		buy_order.setStatus(Status.getStatus(rs.getString("status")));
		buy_order.setTotal(rs.getFloat("total"));
		buy_order.setPaymentNumberFixed(rs.getInt("payment_number_fixed"));
		buy_order.setPaymentNumberFulfilled(rs.getInt("payment_Number_Fulfilled"));
		buy_order.setClientIdClient(rs.getInt("client_Idclient"));
		buy_order.setShipOrderIdShipOrder(rs.getInt("shipOrder_IdShipOrder"));
	
		return buy_order;
	}

	@Override
	public List getAll() throws SQLException {
		String sql = "select * from BuyOrder;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		if(!ps.execute()){
			return null;
		}
		
		ResultSet rs = ps.executeQuery();
		
		ArrayList<BuyOrder> buy_orders = new ArrayList<BuyOrder>();
		while(rs.next()){
			BuyOrder bo = new BuyOrder();
			bo.setIdBuyOrder(rs.getInt("idBuyOrder"));
			bo.setDate(rs.getString("date"));
			bo.setStatus(Status.getStatus(rs.getString("status")));
			bo.setTotal(rs.getFloat("total"));
			bo.setPaymentNumberFixed(rs.getInt("payment_number_fixed"));
			bo.setPaymentNumberFulfilled(rs.getInt("payment_Number_Fulfilled"));
			bo.setClientIdClient(rs.getInt("client_Idclient"));
			bo.setShipOrderIdShipOrder(rs.getInt("shipOrder_IdShipOrder"));
			
			buy_orders.add(bo);
		}
		
		return buy_orders;
	}
	
	public ArrayList<BuyOrder> getClientBuyOrders(int idClient) throws SQLException{
		
		ArrayList<BuyOrder> buyOrders = null;
		String sql = "select * from BuyOrder where Client_idClient = ?;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setInt(1, idClient);
		ResultSet rs = ps.executeQuery();
		
		if(rs.next()){
			rs.previous();
			buyOrders = new ArrayList();
			
			while(rs.next()){
				BuyOrder bo = new BuyOrder();
				bo.setClientIdClient(rs.getInt("Client_idClient"));
				Date dt = rs.getDate("date");
				bo.setDate(dt.toString());
				bo.setIdBuyOrder(rs.getInt("idBuyOrder"));
				bo.setPaymentNumberFixed(rs.getInt("payment_number_fixed"));
				bo.setPaymentNumberFulfilled(rs.getInt("payment_number_fulfilled"));
				bo.setShipOrderIdShipOrder(rs.getInt("ShipOrder_idShipOrder"));
				bo.setStatus(Status.getStatus(rs.getString("status")));
				bo.setTotal(rs.getFloat("total"));
				
				buyOrders.add(bo);
			}
		}
		
		return buyOrders;
	}
	
	/*
	 * This method should be use to retrieve last row inserted on table
	 */
	public int getLastId() throws SQLException{
		String sql = "select idBuyOrder from buyorder order by idBuyOrder desc limit 1;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			return rs.getInt("idBuyOrder");
		}
		
		return 0;
	}
	
}
