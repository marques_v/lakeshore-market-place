package luc.order.dao;

import java.io.IOException;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

import luc.order.model.Payment;
import luc.persistence.DataBase;

public class PaymentDao {

	DataBase db;
	PreparedStatement ps;
	
	public PaymentDao() throws IOException{
		db = new DataBase();
	}
	
	public Payment consult(Object field) throws SQLException{
		
		Payment p = null;
		String query = "SELECT * FROM payment WHERE idPayment = ?;";
		Connection con = db.openConnection();
		
		if(field instanceof Integer){
			ps = con.prepareStatement(query);
			ps.setInt(1, ((Integer) field).intValue());
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				p = new Payment(rs.getString("date"),rs.getFloat("value"),rs.getInt("BuyOrder_idBuyOrder"));
				p.setIdPayment(rs.getInt("idPayment"));
			}
		}
		db.closeConnection();
		return p;
	}
	
	public boolean insert(Payment p) throws SQLException{

		String query = "INSERT INTO payment (date, value, BuyOrder_idBuyOrder) VALUES(?,?,?);";
		Connection con = db.openConnection();
		ps = con.prepareStatement(query);
		ps.setString(1, p.getDate());
		ps.setFloat(2,p.getValue());
		ps.setInt(3,p.getBuyOrderIdBuyOrder());
		ps.execute();
		if(ps.getUpdateCount() <= 0){
			db.closeConnection();
			return false;
		}else{
			db.closeConnection();
			return true;
		}
	}
	
	public List getAll() throws SQLException{
		
		List l = new ArrayList();
		String query = "SELECT * FROM payment;";
		Connection con = db.openConnection();
		ps = con.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()){
			Payment p = new Payment(rs.getString("date"),rs.getFloat("value"),rs.getInt("BuyOrder_idBuyOrder"));
			p.setIdPayment(rs.getInt("idPayment"));
			l.add(p);
		}
		db.closeConnection();
		return l;
	}
	
	public boolean remove(Object field) throws SQLException{
		
		boolean flag = false;
		Connection con = db.openConnection();
		
		if(field instanceof Integer){
			int id = ((Integer) field).intValue();
			if(consult(id).equals(null)){
				return false;
			}else{
				String query = "DELETE FROM payment WHERE idPayment = ?;";
				ps = con.prepareStatement(query);
				ps.setInt(1, id);
				ps.execute();		
				db.closeConnection();
				return true;
			}
		}
		
		db.closeConnection();
		return flag;
	}
	
	public boolean edit(Payment p, Date date, float value, int idBuyOrder) throws SQLException{
		
		boolean flag = false;
		String query = "UPDATE payment SET date = ?, value = ?, BuyOrder_idBuyOrder =? WHERE idPayment = ?;";
		
		//if(!consult(p.getIdPayment()).equals(null)){
			Connection con = db.openConnection();
			ps = con.prepareStatement(query);
			ps.setDate(1, date);
			ps.setFloat(2, value);
			ps.setInt(3, idBuyOrder);
			ps.setInt(4, p.getIdPayment());
			ps.executeUpdate();
			if(ps.getUpdateCount() > 0)
				flag = true;
		//}		
		
		db.closeConnection();
		return flag;
	}
	
}
