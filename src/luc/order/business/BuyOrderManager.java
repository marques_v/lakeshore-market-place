package luc.order.business;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import luc.customer.model.Client;
import luc.order.dao.BuyOrderDao;
import luc.order.dao.PaymentDao;
import luc.order.dao.ShipOrderDao;
import luc.order.model.BuyOrder;
import luc.order.model.Payment;
import luc.order.model.ShipOrder;
import luc.product.dao.ProductDao;
import luc.product.dao.ProductHasBuyOrderDao;
import luc.product.model.Product;
import luc.product.model.ProductHasBuyOrder;

public class BuyOrderManager {
	
	public BuyOrder buy(int product_id, int quantity, int client_id, Float total, Integer paymentNumberFixed, String estimatedArrival){
		DateFormat date_format = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		
		try {
			BuyOrderDao dao = new BuyOrderDao();
			
			ProductDao product_dao = new ProductDao();
			Product product = (Product) product_dao.consult(product_id);
			if(product.getQuantity()>=quantity){
				product.setQuantity(product.getQuantity()-quantity);
				product_dao.edit(product);
				
				ShipOrderDao ship_dao;
				ship_dao = new ShipOrderDao();
				ShipOrder ship_order = new ShipOrder();
				ship_order.setDate(date_format.format(date).toString());
				ship_order.setEstimatedArrival(estimatedArrival);
				ship_order.setStatus(Status.PROCESSING);
				ship_dao.insert(ship_order);
				ship_order.setIdShipOrder(ship_dao.getLastId());
				
				
				BuyOrder buy_order = new BuyOrder();
				buy_order.setClientIdClient(client_id);
				buy_order.setShipOrderIdShipOrder(ship_order.getIdShipOrder());
				buy_order.setDate(date_format.format(date).toString());
				buy_order.setPaymentNumberFixed(paymentNumberFixed);
				buy_order.setPaymentNumberFulfilled(0);
				buy_order.setStatus(Status.PROCESSING);
				buy_order.setTotal(total);
				dao.insert(buy_order);
				buy_order.setIdBuyOrder(dao.getLastId());
				
				ProductHasBuyOrderDao pho_dao = new ProductHasBuyOrderDao();
				ProductHasBuyOrder pho = new ProductHasBuyOrder();
				pho.setBuyorder_idBuyOrder(buy_order.getIdBuyOrder());
				pho.setProduct_idProduct(product_id);
				pho.setQuantity(quantity);
				
				if (pho_dao.insert(pho) ){
					return (BuyOrder) dao.consult(dao.getLastId());
				}
				
			
			}
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	
	public boolean payment(Integer id){
		BuyOrderDao dao ;
		BuyOrder buy_order;
		try {
			dao = new BuyOrderDao();
			buy_order = (BuyOrder) dao.consult(id);
			
			if(buy_order.getPaymentNumberFixed()>buy_order.getPaymentNumberFulfilled()){
				buy_order.setPaymentNumberFulfilled(buy_order.getPaymentNumberFulfilled()+1);
				try {
					
					
					boolean edited = dao.edit(buy_order);
					
					PaymentDao payDao = new PaymentDao();
					Payment payment = new Payment();
					payment.setBuyOrderIdBuyOrder(buy_order.getIdBuyOrder());
					DateFormat date_format = new SimpleDateFormat("yyyy/MM/dd");
					Date date = new Date();
					payment.setDate(date_format.format(date).toString());
					payment.setValue(buy_order.getTotal()/buy_order.getPaymentNumberFixed());
					edited = edited && payDao.insert(payment);
					
					return edited;
				} catch (SQLException | IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		} catch (SQLException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}	
		return false;
	}
	
	public boolean cancelOrder(BuyOrder buy_order){
		if(!buy_order.getStatus().equals(Status.CANCELED)){
			buy_order.setStatus(Status.CANCELED);
			try {
				BuyOrderDao dao = new BuyOrderDao();
				dao.edit(buy_order);
				return true;
			} catch (SQLException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return false;
		
	}
	
	public ArrayList<BuyOrder> getAllBuyOrder(){
		BuyOrderDao dao;
		try {
			dao = new BuyOrderDao();
			ArrayList<BuyOrder> orders = (ArrayList<BuyOrder>) dao.getAll();
			return orders;
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
	}
	
	public BuyOrder retriveve(Integer id){
		BuyOrderDao dao;
		try {
			dao = new BuyOrderDao();
			BuyOrder buyorder = (BuyOrder) dao.consult(id);
			return buyorder;
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
	}
	
	public ArrayList<BuyOrder> getClientBuyOrders(int idClient){
		
		ArrayList<BuyOrder> buyOrders = null;
		
		try {
			BuyOrderDao dao = new BuyOrderDao();
			buyOrders =  dao.getClientBuyOrders(idClient);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return buyOrders;
	}
	
	public boolean changeStatus(Integer id, Status newStatus){
		try {
			BuyOrderDao dao = new BuyOrderDao();
			BuyOrder bo = (BuyOrder) dao.consult(id);
			bo.setStatus(newStatus);
			return dao.edit(bo);
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}	
	}
}