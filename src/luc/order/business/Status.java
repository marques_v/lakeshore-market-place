package luc.order.business;

public enum Status {
	PROCESSING,
	CANCELED,
	DELAYED,
	ACTIVE,
	FINISHED;
	
	public static Status getStatus(String s){
		
		if(s.equals("PROCESSING")){
			return Status.PROCESSING;
		} else if(s.equals("CANCELED")){
			return Status.CANCELED;
		} else if(s.equals("DELAYED")){
			return Status.DELAYED;
		} else if(s.equals("ACTIVE")){
			return Status.ACTIVE;
		} else if(s.equals("FINISHED")){
			return Status.FINISHED;
		}
		return null;
	}
	
	
}

