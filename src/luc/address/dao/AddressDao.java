package luc.address.dao;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

import luc.address.model.Address;
import luc.persistence.DataBase;

public class AddressDao {

	DataBase db;
	PreparedStatement ps;

	public AddressDao() throws IOException {
		db = new DataBase();
	}


	public Address consult(Object field) throws SQLException {
		Address a = null;
		Connection con = db.openConnection();
		if (field instanceof Integer) {
			int id = ((Integer) field).intValue();
			String query = "SELECT * FROM address WHERE idAddress = ?;";
			ps = con.prepareStatement(query);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				a = new Address(rs.getString("address_info_1"), rs.getString("address_info_2"), rs.getString("city"),
						rs.getString("state"), rs.getString("country"));
				a.setIdAddress(rs.getInt("idAddress"));
			}
		} else if (field instanceof String) {
			String info1 = field.toString();
			String query = "SELECT * FROM address WHERE address_info_1 = ?;";
			ps = con.prepareStatement(query);
			ps.setString(1, info1);
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				a = new Address(rs.getString("address_info_1"), rs.getString("address_info_2"), rs.getString("city"),
						rs.getString("state"), rs.getString("country"));
				a.setIdAddress(rs.getInt("idAddress"));
			}
		}
		
		db.closeConnection();
		return a;
	}


	public boolean insert(Address a) throws SQLException {

		String query = "INSERT INTO address (address_info_1, address_info_2, city, state, country) VALUES(?,?,?,?,?);";
		Connection con = db.openConnection();
		ps = con.prepareStatement(query);
		ps.setString(1, a.getAddressInfo1());
		ps.setString(2, a.getAddressInfo2());
		ps.setString(3, a.getCity());
		ps.setString(4, a.getState());
		ps.setString(5, a.getCountry());
		ps.execute();
		db.closeConnection();
		if (consult(a.getAddressInfo1()).equals(null)) {
			return false;
		} else {
			return true;
		}
	}

	public List getAll() throws SQLException {

		String query = "SELECT * FROM address;";

		Connection con = db.openConnection();
		ps = con.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		ArrayList result = new ArrayList();

		while (rs.next()) {
			Address a = new Address();
			a.setIdAddress(rs.getInt("idAddress"));
			a.setAddressInfo1(rs.getString("address_info_1"));
			a.setAddressInfo2(rs.getString("address_info_2"));
			a.setCity(rs.getString("city"));
			a.setState(rs.getString("state"));
			a.setCountry(rs.getString("country"));
			result.add(a);
		}
		db.closeConnection();

		return result;
	}
	
	public boolean remove(Object field) throws SQLException{
		Connection con = db.openConnection();
		
		if(field instanceof Integer){
			int id = ((Integer) field).intValue();
			if (consult(id).equals(null)) {
				return false;
			} else {			
				String query = "DELETE FROM address WHERE idAddress = ?;";
				ps = con.prepareStatement(query);
				ps.setInt(1, id);
				ps.execute();
				return true;
			}
		}else if(field instanceof String){
			String addressInfo = field.toString();
			if (consult(addressInfo).equals(null)) {
				return false;
			} else {
				String query = "DELETE FROM address WHERE address_info_1 = ?;";
				ps = con.prepareStatement(query);
				ps.setString(1, addressInfo);
				ps.execute();
				return true;
			}
		}
		
		db.closeConnection();
		return false;
	}
	
	/*
	public boolean remove(int id) throws SQLException {

		if (consult(id).equals(null)) {
			return false;
		} else {
			String query = "DELETE FROM address WHERE idAddress = ?;";
			Connection con = db.openConnection();
			ps = con.prepareStatement(query);
			ps.setInt(1, id);
			ps.execute();
			db.closeConnection();
			return true;
		}

	}

	public boolean remove(String addressInfo) throws SQLException {

		if (consult(addressInfo).equals(null)) {
			return false;
		} else {
			String query = "DELETE FROM address WHERE address_info_1 = ?;";
			Connection con = db.openConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, addressInfo);
			ps.execute();
			db.closeConnection();
			return true;
		}
	}
	*/

	public boolean edit(Address a, String info1, String info2, String city, String state, String country)
			throws SQLException {

		boolean flag = false;
		String query = "UPDATE address SET address_info_1 = ?, address_info_2 = ?, city = ?, state = ?, country = ? WHERE idAddress = ?;";

		if (!consult(a.getIdAddress()).equals(null)) {
			Connection con = db.openConnection();
			ps = con.prepareStatement(query);
			ps.setString(1, info1);
			ps.setString(2, info2);
			ps.setString(3, city);
			ps.setString(4, state);
			ps.setString(5, country);
			ps.setInt(6, a.getIdAddress());
			ps.executeUpdate();
			flag = true;
			db.closeConnection();
		}

		return flag;
	}
}
