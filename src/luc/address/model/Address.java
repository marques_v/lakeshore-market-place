package luc.address.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Address")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class Address {

	private Integer idAddress;
	private String addressInfo1;
	private String addressInfo2;
	private String city;
	private String state;
	private String country;

	public Address(){}
	
	public Address(String addressInfo1, String addressInfo2, String city, String state, String country) {
		this.addressInfo1 = addressInfo1;
		this.addressInfo2 = addressInfo2;
		this.city = city;
		this.state = state;
		this.country = country;
	}

	public Integer getIdAddress() {
		return this.idAddress;
	}

	public void setIdAddress(Integer idAddress) {
		this.idAddress = idAddress;
	}

	public String getAddressInfo1() {
		return this.addressInfo1;
	}

	public void setAddressInfo1(String addressInfo1) {
		this.addressInfo1 = addressInfo1;
	}

	public String getAddressInfo2() {
		return this.addressInfo2;
	}

	public void setAddressInfo2(String addressInfo2) {
		this.addressInfo2 = addressInfo2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

}
