package luc.address.service.representation;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement(name = "Address")
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
public class AddressRequest {

	private String addressInfo1;
	private String addressInfo2;
	private String city;
	private String state;
	private String country;

	public AddressRequest(){}

	public String getAddressInfo1() {
		return this.addressInfo1;
	}

	public void setAddressInfo1(String addressInfo1) {
		this.addressInfo1 = addressInfo1;
	}

	public String getAddressInfo2() {
		return this.addressInfo2;
	}

	public void setAddressInfo2(String addressInfo2) {
		this.addressInfo2 = addressInfo2;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return this.state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return this.country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
