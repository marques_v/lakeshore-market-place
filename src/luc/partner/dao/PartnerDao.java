package luc.partner.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import luc.partner.model.Partner;
import luc.persistence.Dao;
import luc.persistence.DataBase;

public class PartnerDao implements Dao{
	
	private Connection connection;
	
	public PartnerDao() throws IOException{
		DataBase db = new DataBase();
		connection = db.openConnection();
	}
	
	@Override
	public boolean insert(Object o) throws SQLException{
		String sql = "insert into partner (company_name, contract_date, user_name, user_password) values (?,?,?,?);";
        
		if(!o.getClass().equals(Partner.class)){
			return false;
		}
		Partner partner = (Partner) o;
		
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ps.setString(1, partner.getCompanyName());
		ps.setString(2, partner.getContractDate());
		ps.setString(3, partner.getUserName());
		ps.setString(4, partner.getUserPassword());
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public boolean edit(Object o) throws SQLException {
		String sql = "update partner set company_name=?,contract_date=?,user_name=? where idPartner=?;";
		
		if(!o.getClass().equals(Partner.class)){
			return false;
		}
		
		Partner partner = (Partner) o;
		
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ps.setString(1, partner.getCompanyName());
		ps.setString(2, partner.getContractDate());
		ps.setString(3, partner.getUserName());
		ps.setInt(4, partner.getIdPartner());
		
		return ps.executeUpdate()!=0;
	}
	
	public boolean editPassword(int id, String password) throws SQLException{
		String sql = "update partner set user_password=sha1(?) where idPartner=?;";
	
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setString(1, password);
		ps.setInt(2, id);
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public boolean remove(Object o) throws SQLException {
		String sql;
		PreparedStatement ps; 
		
		// This method only accepts Strings as company_name or the idPartner itself 
		if(o.getClass().equals(String.class)){
			String key_value = (String) o;
			sql = "delete from partner where company_name=? limit 1;";
			ps = (PreparedStatement) connection.prepareStatement(sql);
			ps.setString(1, key_value);
		} else if(o.getClass().equals(Integer.class)){
			Integer key_value = (Integer) o;
			sql = "delete from partner where idPartner=? limit 1;";
			ps = (PreparedStatement) connection.prepareStatement(sql);
			ps.setInt(1, key_value);
		} else{
			return false;
		}
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public Object consult(Object o) throws SQLException{
		String sql;
		PreparedStatement ps; 
		
		// This method only accepts Strings as company_name or the idPartner itself 
		if(o.getClass().equals(String.class)){
			String key_value = (String) o;
			sql = "select * from partner where company_name=?;";
			ps = (PreparedStatement) connection.prepareStatement(sql);
			ps.setString(1, key_value);
		} else if(o.getClass().equals(Integer.class)){
			Integer key_value = (Integer) o;
			sql = "select * from partner where idPartner=?;";
			ps = (PreparedStatement) connection.prepareStatement(sql);
			ps.setInt(1, key_value);
		} else{
			return null;
		}
		
		if(!ps.execute()){
			return null;
		}
		
		ResultSet rs = ps.executeQuery();
		if(!rs.next()){
			return null;
		}
		
		Partner partner = new Partner();
		partner.setCompanyName(rs.getString("company_name"));
		partner.setContractDate(rs.getString("contract_date"));
		partner.setIdPartner(rs.getInt("idPartner"));
		partner.setUserName(rs.getString("user_name"));
		partner.setUserPassword(rs.getString("user_password"));
		
		return partner;
	}
	
	public Partner consultByUserName(String userName) throws SQLException{
		
		String sql = "select * from partner where user_name=?;";
		PreparedStatement ps;
		ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setString(1, userName);
		
		if(!ps.execute()){
			return null;
		}
		
		ResultSet rs = ps.executeQuery();
		if(!rs.next()){
			return null;
		}
		
		Partner partner = new Partner();
		partner.setCompanyName(rs.getString("company_name"));
		partner.setContractDate(rs.getString("contract_date"));
		partner.setIdPartner(rs.getInt("idPartner"));
		partner.setUserName(rs.getString("user_name"));
		partner.setUserPassword(rs.getString("user_password"));
		
		return partner;	
	}

	@Override
	public List getAll() throws SQLException {
		String sql = "select * from partner;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		if(!ps.execute()){
			return null;
		}
		
		ResultSet rs = ps.executeQuery();
		
		ArrayList<Partner> partners = new ArrayList<Partner>();
		while(rs.next()){
			Partner p = new Partner();
			p.setIdPartner(rs.getInt("idPartner"));
			p.setCompanyName(rs.getString("company_name"));
			p.setContractDate(rs.getString("contract_date"));
			p.setUserName(rs.getString("user_name"));
			
			partners.add(p);
		}
		
		return partners;
	}
	
	/*
	 * This method should be use to retrieve last row inserted on table
	 */
	public int getLastId() throws SQLException{
		String sql = "select idPartner from partner order by idPartner desc limit 1;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ResultSet rs = ps.executeQuery();
		if(rs.next()){
			return rs.getInt("idPartner");
		}
		
		return 0;
	}
}