package luc.partner.model;

import java.io.Serializable;

public class Partner implements Serializable{

	private static final long serialVersionUID = 1L;
	private Integer idPartner;
	private String companyName;
	private String contractDate;
	private String userName;
	private String userPassword;

	public Partner() {
	}

	public Partner(String companyName, String contractDate, String userName, String userPassword) {
		this.companyName = companyName;
		this.contractDate = contractDate;
		this.userName = userName;
		this.userPassword = userPassword;
	}

	public Integer getIdPartner() {
		return this.idPartner;
	}

	public void setIdPartner(Integer idPartner) {
		this.idPartner = idPartner;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getContractDate() {
		return this.contractDate;
	}

	public void setContractDate(String contractDate) {
		this.contractDate = contractDate;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

}
