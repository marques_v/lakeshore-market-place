package luc.partner.service;

import javax.jws.WebService;
import javax.ws.rs.core.Response;

import luc.partner.service.representation.PartnerRepresentation;
import luc.partner.service.representation.PartnerRequest;

import java.util.Set;

@WebService
public interface PartnerService {
	public Set<PartnerRepresentation> getPartners();
	public PartnerRepresentation getPartner(Integer idPartner);
	public PartnerRepresentation createPartner(PartnerRequest partnerRequest);
	public Response deletePartner(Integer id);
}
