package luc.partner.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import luc.partner.dao.PartnerDao;
import luc.partner.service.representation.PartnerRepresentation;
import luc.partner.service.representation.PartnerRequest;
import luc.partner.service.workflow.PartnerActivity;

@Path("/partnerservice/")
public class PartnerResource implements PartnerService{
	private PartnerActivity parActivity;
	
	public PartnerResource() {
		parActivity = new PartnerActivity();
	}

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/partner")
	public Set<PartnerRepresentation> getPartners() {
		System.out.println("GET METHOD Request for all partners .............");
		return parActivity.getPartners();
	}

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/partner/{idPartner}")
	public PartnerRepresentation getPartner(@PathParam("idPartner") Integer idPartner) {
		System.out.println("GET METHOD Request for a particular partner .............");
		return parActivity.getPartner(idPartner);
	}

	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/partner/")
	public PartnerRepresentation createPartner(PartnerRequest partnerRequest) {
		System.out.println("GET METHOD Request for a particular partner ............."+partnerRequest.getCompanyName()+" "+partnerRequest.getUserName());
		return parActivity.createPartner(partnerRequest);
	}
	
	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/partner/{idPartner}")
	public Response deletePartner(@PathParam("idPartner") Integer id){
		System.out.println("Delete METHOD Request from Client with partner String ............." + id);
		if(!parActivity.deletePartner(id)) { 
			return Response.status(Status.OK).build(); 
		}
		return null;
	}
}
