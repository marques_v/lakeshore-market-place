package luc.partner.service.workflow;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import luc.link.model.Link;
import luc.partner.business.PartnerManager;
import luc.partner.dao.PartnerDao;
import luc.partner.model.Partner;
import luc.partner.service.representation.PartnerRepresentation;
import luc.partner.service.representation.PartnerRequest;
import luc.product.business.ProductManager;
import luc.product.model.Product;

public class PartnerActivity {
	private static PartnerManager pmanager;
	
	public PartnerActivity() {
		pmanager = new PartnerManager();
	}
	
	public Set<PartnerRepresentation> getPartners(){
		ArrayList<Partner> partners = pmanager.getPartners();
		Set<PartnerRepresentation> partnerRepresentations = new HashSet<PartnerRepresentation>();
		
		Iterator i = partners.iterator();
		while(i.hasNext()){
			Partner p = (Partner) i.next();
			PartnerRepresentation pr = new PartnerRepresentation();
			pr.setCompanyName(p.getCompanyName());
			pr.setContractDate(p.getContractDate());
			pr.setIdPartner(p.getIdPartner());
			pr.setUserName(p.getUserName());
			
			partnerRepresentations.add(pr);
		}
		
		return partnerRepresentations;
	}
	
	public PartnerRepresentation getPartner(Integer id){
		Partner partner;
		PartnerRepresentation partnerRepresentation = new PartnerRepresentation();
		try {
			partner = (Partner) pmanager.getPartner(id);
			
			partnerRepresentation.setCompanyName(partner.getCompanyName());
			partnerRepresentation.setContractDate(partner.getContractDate());
			partnerRepresentation.setIdPartner(partner.getIdPartner());
			partnerRepresentation.setUserName(partner.getUserName());
			
			setLinks(partnerRepresentation);
			
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return partnerRepresentation;
	}
	
	public PartnerRepresentation createPartner(PartnerRequest partnerRequest){
		Partner partner = pmanager.addPartner(partnerRequest.getCompanyName(), partnerRequest.getUserName(), partnerRequest.getUserPassword());
		PartnerRepresentation prepresentation = new PartnerRepresentation();
		prepresentation.setCompanyName(partner.getCompanyName());
		prepresentation.setContractDate(partner.getContractDate());
		prepresentation.setIdPartner(partner.getIdPartner());
		prepresentation.setUserName(partner.getUserName());
		return prepresentation;
	}
	
	public boolean deletePartner(Integer id){
		try {
			return pmanager.deletePartner(id);
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public void setLinks(PartnerRepresentation rep){
		
		ArrayList<Link> links = new ArrayList();
		
		Link delete = new Link("delete","http://localhost:8090/partnerservice/partner/"+
		rep.getIdPartner());
		links.add(delete);
		
		try {
			ProductManager pm = new ProductManager();
			ArrayList<Product> products = pm.getPartnerProducts(rep.getIdPartner());
			
			if(products != null){
				for(Product p : products){
					Link view = new Link("view","http://localhost:8090/productservice/product/"+
							p.getIdProduct());
					links.add(view);
				}
			}
			
		} catch (IOException | SQLException e) {
			e.printStackTrace();
		}
		
		rep.setLinkList(links);
	}
}
