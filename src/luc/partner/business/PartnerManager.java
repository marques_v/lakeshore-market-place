package luc.partner.business;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import luc.order.dao.BuyOrderDao;
import luc.order.model.BuyOrder;
import luc.partner.dao.PartnerDao;
import luc.partner.model.Partner;
import luc.product.dao.ProductDao;
import luc.product.model.Product;

public class PartnerManager {
	
	public Partner addPartner(String companyName, String userName, String userPassword){
		PartnerDao dao;
		Partner partner = new Partner();
		partner.setCompanyName(companyName);
		partner.setUserName(userName);
		partner.setUserPassword(userPassword);
		DateFormat date_format = new SimpleDateFormat("yyyy/MM/dd");
		Date date = new Date();
		partner.setContractDate(date_format.format(date).toString());
		
		try {
			dao = new PartnerDao();
			if( dao.insert(partner) ){
				return (Partner) dao.consult(dao.getLastId());
			}
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	/*
	 * When partner account is to be deleted, this method must verify if ther's any
	 * buy order active
	 *	
	 * ##### Not implemented yet ####
	 */
	public <T> boolean deletePartner(T companyName) throws IOException, SQLException{
		PartnerDao dao = new PartnerDao();
		return dao.remove(companyName);
	}
	
	public Partner getPartner(Object o) throws SQLException, IOException{
		PartnerDao dao = new PartnerDao();
		return (Partner) dao.consult(o);
	}
	
	public ArrayList<Partner> getPartners(){
		PartnerDao dao;
		try {
			dao = new PartnerDao();
			ArrayList<Partner> partners = (ArrayList<Partner>) dao.getAll();
			return partners;
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	public boolean addProductFromPartner(Partner partner, Product product) throws IOException, SQLException{
		product.setPartnerIdPartner(partner.getIdPartner());
		ProductDao product_dao = new ProductDao();
		return product_dao.insert(product);
	}
	
}
