package luc.product.business;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.hamcrest.core.IsNot;

import luc.product.dao.ProductDao;
import luc.product.model.Product;

public class ProductManager {
	private ProductDao dao;
	
	public ProductManager() throws IOException{
		this.dao = new ProductDao();
	}

	public ArrayList<Product> getPartnerProducts(int partnerId) throws SQLException{
		ArrayList<Product> product_list = (ArrayList<Product>) dao.getAll();
		ArrayList<Product> partner_product = new ArrayList<Product>();
		Iterator<Product> i = product_list.iterator();
		
		while(i.hasNext()){
			Product product = i.next();
			if(product.getPartnerIdPartner()==partnerId){
				partner_product.add(product);
			}
		}
		
		return partner_product;
	}
	
	public boolean checkViability(Object o) throws SQLException{
		Product product = (Product) dao.consult(o);
		if(product!=null){
			return product.getQuantity()>0;
		}
		return false;
	}
	
	public ArrayList<Product> searchProduct(String name) throws SQLException{
		ArrayList<Product> list = (ArrayList<Product>) dao.getAll();
		Iterator<Product> i = list.iterator();
		ArrayList<Product> search_results = new ArrayList<Product>();
		while(i.hasNext()){
			Product product = i.next();
			if(product.getName().contains(name)){
				search_results.add(product);
			}
		}
		return search_results;
	}
	
	public Product getProductById(Integer id) throws SQLException{
		return (Product) dao.consult(id);
	}
}
