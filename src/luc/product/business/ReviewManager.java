package luc.product.business;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;

import luc.customer.model.Client;
import luc.persistence.DataBase;
import luc.product.dao.ReviewDao;
import luc.product.model.Product;
import luc.product.model.Review;

public class ReviewManager{
	
	private Client client;
	private DataBase db;
	private PreparedStatement ps;
	private ReviewDao rd;
	
	public ReviewManager() throws IOException{
		this.db = new DataBase();
		this.rd = new ReviewDao();
	}
	
	public boolean addReview(Client client, Product product, String text) throws IOException, SQLException{
		
		boolean flag = true;
		Review review = new Review(text,product.getIdProduct(),client.getIdClient());
		boolean insertion = rd.insert(review);
		
		if(insertion == false){
			throw new SQLException("Could not add the review");
		}

		return flag;
	}
	
	public boolean removeReview(int idReview) throws SQLException{
		
		return rd.remove(new Integer(idReview));
		
	}
	
	public boolean editReview(Review r) throws SQLException{
		
		boolean flag = false;
		ArrayList<Review> l = (ArrayList<Review>) rd.getAll();
		
		for(Review rv : l){
			if(rv.getIdProduct() == r.getIdProduct() && rv.getIdClient() == r.getIdClient()){
				//r.setId(rv.getId());
				flag = rd.edit(rv, r.getText(), r.getIdProduct());
				break;
			}
		}
		
		return flag;
	}
	
	public ArrayList<Review> getReviewsProduct(int idProduct) throws SQLException{
		
		ArrayList<Review> l = (ArrayList<Review>) rd.getAll();
		ArrayList<Review> reviews = new ArrayList();
		
		for(Review r : l){
			if(r.getIdProduct() == idProduct)
				reviews.add(r);
		}
		
		return reviews;
	}
	
public ArrayList<Review> getReviewsClient(int idClient) throws SQLException{
		
		ArrayList<Review> l = (ArrayList<Review>) rd.getAll();
		ArrayList<Review> reviews = new ArrayList();
		
		for(Review r : l){
			if(r.getIdClient() == idClient)
				reviews.add(r);
		}
		
		return reviews;
	}
	
}