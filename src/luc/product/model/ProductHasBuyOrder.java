package luc.product.model;

public class ProductHasBuyOrder {
	private int product_idProduct;
	private int buyorder_idBuyOrder;
	private int quantity;
	
	
	public int getProduct_idProduct() {
		return product_idProduct;
	}
	public void setProduct_idProduct(int product_idProduct) {
		this.product_idProduct = product_idProduct;
	}
	public int getBuyorder_idBuyOrder() {
		return buyorder_idBuyOrder;
	}
	public void setBuyorder_idBuyOrder(int buyorder_idBuyOrder) {
		this.buyorder_idBuyOrder = buyorder_idBuyOrder;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	
}
