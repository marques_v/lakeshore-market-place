package luc.product.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Product implements Serializable{
	
	private static final long serialVersionUID = 1L;	
	private Integer idProduct;
	private String name;
	private Float price;
	private int partnerIdPartner;
	private int quantity;

	public Product() {
	}

	public Product(int partnerIdPartner) {
		this.partnerIdPartner = partnerIdPartner;
	}

	public Product(String name, Float price, int partnerIdPartner) {
		this.name = name;
		this.price = price;
		this.partnerIdPartner = partnerIdPartner;
	}

	public Integer getIdProduct() {
		return this.idProduct;
	}

	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Float getPrice() {
		return this.price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}

	public int getPartnerIdPartner() {
		return this.partnerIdPartner;
	}

	public void setPartnerIdPartner(int partnerIdPartner) {
		this.partnerIdPartner = partnerIdPartner;
	}
	
	public void setQuantity(int quantity){
		this.quantity = quantity;
	}
	
	public int getQuantity(){
		return this.quantity;
	}

}
