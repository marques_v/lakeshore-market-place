package luc.product.model;

public class Review {
	
	private int id;
	private String text;
	private int idProduct;
	private int idClient;
	
	public Review(){
		
	}
	
	public Review(String text, int idProduct, int idClient){
		this.text = text;
		this.idProduct = idProduct;
		this.idClient = idClient;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
	
	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int id) {
		this.idProduct = id;
	}
	
	public int getIdClient() {
		return idClient;
	}

	public void setIdClient(int id) {
		this.idClient = id;
	}
	
}
