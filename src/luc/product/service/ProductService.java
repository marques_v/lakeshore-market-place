package luc.product.service;

import java.util.Set;

import javax.jws.WebService;

import luc.product.service.representation.ProductRepresentation;
import luc.product.service.representation.ProductRequest;

@WebService
public interface ProductService {
	public Set<ProductRepresentation> getProducts();
	public ProductRepresentation getProduct(Integer idProduct);
	public ProductRepresentation createProduct(ProductRequest productRequest);
	public Set<ProductRepresentation> searchProducts(String name);
}
