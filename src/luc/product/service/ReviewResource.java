package luc.product.service;

import java.util.Set;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import luc.product.service.representation.ReviewRepresentation;
import luc.product.service.representation.ReviewRequest;
import luc.product.service.workflow.ReviewActivity;

@Path("/marketservice/")
public class ReviewResource implements ReviewService{

	public ReviewResource(){}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/reviewp/{productId}")
	public Set<ReviewRepresentation> getReviewsProduct(@PathParam("productId")int id) {
		ReviewActivity a = new ReviewActivity();
		return a.getReviewsProduct(id);
	}

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/reviewc/{clientId}")
	public Set<ReviewRepresentation> getReviewsClient(@PathParam("clientId") int id) {
		ReviewActivity a = new ReviewActivity();
		return a.getReviewsClient(id);
	}

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/review/{reviewId}")
	public ReviewRepresentation getReview(@PathParam("reviewId") int id) {
		ReviewActivity a = new ReviewActivity();
		return a.getReview(id);
	}

	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/review")
	public ReviewRepresentation createReview(ReviewRequest r) {
		ReviewActivity a = new ReviewActivity();
		return a.createReview(r);
	}

	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/review/{reviewId}")
	public Response deleteReview(@PathParam("reviewId") int id) {
		ReviewActivity a = new ReviewActivity();
		String res = a.deleteReview(id);
		if (res.equals("OK")) {
			return Response.status(Status.OK).build();
		}
		return null;
	}

	@PUT
	@Produces({"application/xml" , "application/json"})
	@Path("/review")
	public Response updateReview(ReviewRequest r) {
		ReviewActivity a = new ReviewActivity();
		String res = a.updateReview(r);
		if (res.equals("OK")) {
			return Response.status(Status.OK).build();
		}
		return null;
	}

}
