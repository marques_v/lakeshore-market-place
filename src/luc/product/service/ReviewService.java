package luc.product.service;

import java.util.Set;

import javax.ws.rs.core.Response;

import luc.product.service.representation.ReviewRepresentation;
import luc.product.service.representation.ReviewRequest;



public interface ReviewService {
	
	Set<ReviewRepresentation> getReviewsProduct(int id);
	Set<ReviewRepresentation> getReviewsClient(int id);
	ReviewRepresentation getReview(int id);
	ReviewRepresentation createReview(ReviewRequest r);
	Response deleteReview(int id);
	Response updateReview(ReviewRequest c);
}
