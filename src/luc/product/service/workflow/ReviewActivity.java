package luc.product.service.workflow;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import javax.ws.rs.core.Response;

import luc.customer.dao.ClientDao;
import luc.customer.model.Client;
import luc.link.model.Link;
import luc.product.business.ProductManager;
import luc.product.business.ReviewManager;
import luc.product.dao.ReviewDao;
import luc.product.model.Product;
import luc.product.model.Review;
import luc.product.service.representation.ReviewRepresentation;
import luc.product.service.representation.ReviewRequest;

public class ReviewActivity {

	private ReviewDao rd;
	private ReviewManager rm;
	
	public ReviewActivity(){
		try {
			this.rm = new ReviewManager();
			this.rd = new ReviewDao();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	public Set<ReviewRepresentation> getReviewsProduct(int id){
		
		ArrayList<Review> al = new ArrayList();
		Set<ReviewRepresentation> s = new HashSet();
		
		try {
			al = rm.getReviewsProduct(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Iterator it = al.iterator();
		
		while(it.hasNext()){
			Review r = (Review) it.next();
			ReviewRepresentation rep = new ReviewRepresentation();
			rep.setId(r.getId());
			rep.setIdClient(r.getIdClient());
			rep.setIdProduct(r.getIdProduct());
			rep.setText(r.getText());
			s.add(rep);
		}
		
		return s;
	}
	
public Set<ReviewRepresentation> getReviewsClient(int id){
		
		ArrayList<Review> al = new ArrayList();
		Set<ReviewRepresentation> s = new HashSet();
		
		try {
			al = rm.getReviewsClient(id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		Iterator it = al.iterator();
		
		while(it.hasNext()){
			Review r = (Review) it.next();
			ReviewRepresentation rep = new ReviewRepresentation();
			rep.setId(r.getId());
			rep.setIdClient(r.getIdClient());
			rep.setIdProduct(r.getIdProduct());
			rep.setText(r.getText());
			s.add(rep);
		}
		
		return s;
	}
	
	public ReviewRepresentation getReview(int id){
		
		ReviewRepresentation rep = null;
		try {
			Review r = rd.consult(new Integer(id));
			if(r != null){
				rep = new ReviewRepresentation();
				rep.setId(r.getId());
				rep.setIdClient(r.getIdClient());
				rep.setIdProduct(r.getIdProduct());
				rep.setText(r.getText());
				
				setLinks(rep);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return rep;
	}
	
	public ReviewRepresentation createReview(ReviewRequest request){
		
		ReviewRepresentation rep = null;
		
		try {
			ProductManager pm = new ProductManager();
			ClientDao cd = new ClientDao();
			Product p = pm.getProductById(request.getIdProduct());
			Client c = cd.consult(new Integer(request.getIdClient()));
			
			if(rm.addReview(c, p, request.getText())){
				rep = new ReviewRepresentation();
				rep.setIdClient(c.getIdClient());
				rep.setIdProduct(p.getIdProduct());
				rep.setText(request.getText());
				Review r = rd.consult(request.getText());
				rep.setId(r.getId());
			}
			
		} catch (SQLException | IOException e1) {
			e1.printStackTrace();
		}
		
		return rep;
	}

	public String deleteReview(int id){
		
		String response = "NOT OK";
		try {
			
			if(rm.removeReview(id)){
				return "OK";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	public String updateReview(ReviewRequest request){
		
		String response = "NOT OK";
		try {
			Review r = rd.consult(request.getIdClient(),request.getIdProduct());
			if(r != null){
				r.setText(request.getText());
				if(rm.editReview(r))
					return "OK";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
			
		return response;
	}
	
	public void setLinks(ReviewRepresentation rep){
		
		Link delete = new Link("delete","http://localhost:8090/marketservice/review/"+
				rep.getId());
		Link edit = new Link("edit","http://localhost:8090/marketservice/review");	
		Link buy = new Link("buy","http://localhost:8090/buyorderservice/buyorder");
		
		rep.setLinks(delete,edit,buy);
	}
	
}
