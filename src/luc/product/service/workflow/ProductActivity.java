package luc.product.service.workflow;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import luc.link.model.Link;
import luc.login.business.LoginManager;
import luc.partner.model.Partner;
import luc.partner.service.representation.PartnerRepresentation;
import luc.product.business.ProductManager;
import luc.product.dao.ProductDao;
import luc.product.model.Product;
import luc.product.service.representation.ProductRepresentation;
import luc.product.service.representation.ProductRequest;

public class ProductActivity {
	private static ProductDao pdao;
	private ProductManager manager;
	
	public ProductActivity() throws IOException{
		pdao = new ProductDao();
		manager = new ProductManager();
	}
	
	public Set<ProductRepresentation> getProducts() throws SQLException{
		ArrayList<Product> products = (ArrayList<Product>) pdao.getAll();
		Set<ProductRepresentation> productRepresentations = new HashSet<ProductRepresentation>();
		
		Iterator i = products.iterator();
		while(i.hasNext()){
			Product p = (Product) i.next();
			ProductRepresentation pr = new ProductRepresentation();
			pr.setName(p.getName());
			pr.setPrice(p.getPrice());
			pr.setQuantity(p.getQuantity());
			pr.setIdProduct(p.getIdProduct());
			pr.setPartnerIdPartner(p.getPartnerIdPartner());
			
			productRepresentations.add(pr);
		}
		return productRepresentations;
	}
	
	public ProductRepresentation getProduct(Integer id){
		Product product;
		ProductRepresentation productRepresentation = new ProductRepresentation();
		
		try{
			product = (Product) pdao.consult(id);
			
			productRepresentation.setIdProduct(product.getIdProduct());
			productRepresentation.setName(product.getName());
			productRepresentation.setPrice(product.getPrice());
			productRepresentation.setQuantity(product.getQuantity());
			productRepresentation.setPartnerIdPartner(product.getPartnerIdPartner());
			
			setGetLinks(productRepresentation);
			
		} catch(SQLException e){
			e.printStackTrace();
		}
		return productRepresentation;
	}
	
	public Set<ProductRepresentation> searchProducts(String name){
		
		Set<ProductRepresentation> productRepresentations = new HashSet<ProductRepresentation>();
		ArrayList<Product> products;
		
		try {
			
			products = manager.searchProduct(name);
			Iterator i = products.iterator();
			
			while(i.hasNext()){
				Product p = (Product) i.next();
				ProductRepresentation pr = new ProductRepresentation();
				pr.setName(p.getName());
				pr.setPrice(p.getPrice());
				pr.setQuantity(p.getQuantity());
				pr.setIdProduct(p.getIdProduct());
				pr.setPartnerIdPartner(p.getPartnerIdPartner());
				productRepresentations.add(pr);
				
				setGetLinks(pr);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return productRepresentations;
	}
	
	public ProductRepresentation createProduct(ProductRequest productRequest){
		Product product = new Product();
		ProductRepresentation prepresentation = new ProductRepresentation();
		
		product.setName(productRequest.getName());
		product.setPrice(productRequest.getPrice());
		product.setQuantity(productRequest.getQuantity());
		product.setPartnerIdPartner(productRequest.getPartnerIdPartner());
		
		prepresentation.setName(productRequest.getName());
		prepresentation.setPrice(productRequest.getPrice());
		prepresentation.setQuantity(productRequest.getQuantity());
		prepresentation.setPartnerIdPartner(productRequest.getPartnerIdPartner());
		
		try{
			pdao.insert(product);
		} catch(SQLException e){
			e.printStackTrace();
		}
		return prepresentation;
	}
	
	public String delectProduct(int id){
		Product p = null;
		String response = "NOT OK";
		
		try{
			pdao.remove(new Integer(id));
			response = "OK";
		} catch(SQLException e){
			e.printStackTrace();
		}
		return response;
	}
	
	public String updateProduct(ProductRequest productRequest){
		
		String response = "NOT OK";
		Product product = new Product();
		
		product.setName(productRequest.getName());
		product.setPrice(productRequest.getPrice());
		product.setQuantity(productRequest.getQuantity());
		product.setIdProduct(productRequest.getPartnerIdPartner());
		
		try{
			pdao.edit(product);
			response = "OK";
		} catch(SQLException e){
			e.printStackTrace();
		}
		
		return response;
	}
	
	// links for the get GET method response
	public void setGetLinks(ProductRepresentation rep){
		
		Link buy = new Link("buy", "http://localhost:8090/buyorderservice/buyorder");
		Link writeReview = new Link("write review","http://localhost:8090/marketservice/review");
		Link showReviews = new Link("show reviews","http://localhost:8090/marketservice/reviewp/"+
				rep.getIdProduct());
		
		LoginManager lm = new LoginManager();
		Partner partner = lm.getLoggedPartner();
		
		if(partner != null){
			//If the Partner is the owner of the product
			if(rep.getPartnerIdPartner() == partner.getIdPartner()){
				Link delete = new Link("delete", "http://localhost:8090/productservice/product/"+ 
						rep.getIdProduct());
				
				rep.setLinks(buy,writeReview,showReviews,delete);
			}
		}
		rep.setLinks(buy,writeReview,showReviews);
	}
}
