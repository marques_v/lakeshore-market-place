package luc.product.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Set;

import javax.websocket.server.PathParam;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;


import luc.product.service.representation.ProductRepresentation;
import luc.product.service.representation.ProductRequest;
import luc.product.service.workflow.ProductActivity;

@Path("/productservice/")
public class ProductResource implements ProductService{
	private ProductActivity proActivity;
	
	public ProductResource(){
		try {
			proActivity = new ProductActivity();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/products")
	public Set<ProductRepresentation> getProducts() {
		Set<ProductRepresentation> p = null;
		System.out.println("GET METHOD Request for all products .............");
		try {
			p = proActivity.getProducts();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return p;
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/product/{idProduct}")
	public ProductRepresentation getProduct(@PathParam("idProduct") Integer idProduct) {
		System.out.println("GET METHOD Request for a particular product .............");
		return proActivity.getProduct(idProduct);
	}
	
	@GET
	@Produces({"application/xml" , "application/json"})
	@Path("/search/product")
	public Set<ProductRepresentation> searchProducts(@QueryParam("name") String name) {
		return proActivity.searchProducts(name);
	}
	
	@POST
	@Produces({"application/xml" , "application/json"})
	@Path("/product/")
	public ProductRepresentation createProduct(ProductRequest productRequest) {
		System.out.println("GET METHOD Request for a particular product ............."+productRequest.getName()+" "+productRequest.getPrice());
		return proActivity.createProduct(productRequest);
	}
	
	@DELETE
	@Produces({"application/xml" , "application/json"})
	@Path("/product/{idProduct}")
	public Response deleteProduct(@PathParam("idProduct") Integer id){
		System.out.println("Delete METHOD Request from Client with product String ............." + id);
		if(!proActivity.delectProduct(id).equals("NOT OK")) { 
			return Response.status(Status.OK).build();
		}
		return null;
	}
	
	@PUT
	@Produces({"application/xml" , "application/json"})
	@Path("/product/{productId}")
	public Response updateProduct(ProductRequest productRequest) throws IOException{
		ProductActivity pa = new ProductActivity();
		String res = pa.updateProduct(productRequest);
		
		if(!res.equals("NOT OK")){
			return Response.status(Status.OK).build();
		}
		return null;
	}
	
	
}