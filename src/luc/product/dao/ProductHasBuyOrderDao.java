package luc.product.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import luc.partner.model.Partner;
import luc.persistence.Dao;
import luc.persistence.DataBase;
import luc.product.model.ProductHasBuyOrder;

public class ProductHasBuyOrderDao implements Dao{
	private Connection connection;
	
	public ProductHasBuyOrderDao() throws IOException{
		DataBase db = new DataBase();
		connection = db.openConnection();
	}

	@Override
	public boolean insert(Object o) throws SQLException {
		if(o.getClass().equals(ProductHasBuyOrder.class)){
			String sql = "insert into product_has_buyorder (product_idProduct, buyorder_idBuyOrder, quantity) values (?,?,?);";
			
			ProductHasBuyOrder pho = (ProductHasBuyOrder) o;
			PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
			
			ps.setInt(1, pho.getProduct_idProduct());
			ps.setInt(2, pho.getBuyorder_idBuyOrder());
			ps.setInt(3, pho.getQuantity());
			
			return ps.executeUpdate()!=0;
		}
		return false;
	}

	@Override
	public boolean edit(Object o) throws SQLException {
		if(o.getClass().equals(ProductHasBuyOrder.class)){
			String sql = "update product_has_buyorder set quantity=? where product_idProduct=? and buyorder_idBuyOrder=?;";
			
			ProductHasBuyOrder pho = (ProductHasBuyOrder) o;
			
			PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
			
			ps.setInt(1, pho.getQuantity());
			ps.setInt(2, pho.getProduct_idProduct());
			ps.setInt(3, pho.getBuyorder_idBuyOrder());
			
			return ps.executeUpdate()!=0;
		}
		return false;
	}

	@Override
	public boolean remove(Object o) throws SQLException {
		if(o.getClass().equals(ProductHasBuyOrder.class)){
			String sql = "delete from product_has_buyorder where product_idProduct=? and buyorder_idBuyOrder=?;";
			
			ProductHasBuyOrder pho = (ProductHasBuyOrder) o;
			
			PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
			
			ps.setInt(1, pho.getProduct_idProduct());
			ps.setInt(2, pho.getBuyorder_idBuyOrder());
			
			return ps.executeUpdate()!=0;
		}
		return false;
	}

	@Override
	public Object consult(Object o) throws SQLException {		
		return null;
	}
	
	public ProductHasBuyOrder consult(int[] ids) throws SQLException{
		if(ids.length==2){
			String sql = "select * from product_has_buyorder where product_idProduct=? and buyorder_idBuyOrder=?;";
			PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
			
			ps.setInt(1, ids[0]);
			ps.setInt(2, ids[1]);
			
			ResultSet rs = ps.executeQuery();
			rs.next();
			
			ProductHasBuyOrder pho = new ProductHasBuyOrder();
			pho.setBuyorder_idBuyOrder(rs.getInt("buyorder_idBuyOrder"));
			pho.setProduct_idProduct(rs.getInt("product_idProduct"));
			pho.setQuantity(rs.getInt("quantity"));
			return pho;
		}
		return null;
	}

	@Override
	public List getAll() throws SQLException {
		String sql = "select * from product_has_buyorder;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		
		if(ps.execute()){
			ResultSet rs = ps.executeQuery();
			
			ArrayList<ProductHasBuyOrder> products = new ArrayList<ProductHasBuyOrder>();
			while(rs.next()){
				ProductHasBuyOrder pho = new ProductHasBuyOrder();
				pho.setBuyorder_idBuyOrder(rs.getInt("buyorder_idBuyOrder"));
				pho.setProduct_idProduct(rs.getInt("product_idProduct"));
				pho.setQuantity(rs.getInt("quantity"));
				products.add(pho);
			}
			return products;
		}
		return null;
	}

}
