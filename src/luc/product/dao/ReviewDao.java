package luc.product.dao;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

import luc.persistence.DataBase;
import luc.product.model.Review;

public class ReviewDao {

	private DataBase db;
	private PreparedStatement ps;
	
	public ReviewDao() throws IOException{
		this.db = new DataBase();
	}
	
	public boolean insert(Review r) throws SQLException{
		
		boolean flag = false;
		String query = "INSERT INTO review (text,Product_idProduct,Client_idClient) VALUES(?,?,?);";
		Connection con = db.openConnection();
		ps = con.prepareStatement(query);
		ps.setString(1,r.getText());
		ps.setInt(2, r.getIdProduct());
		ps.setInt(3, r.getIdClient());
		ps.execute();
	
		if(ps.getUpdateCount()<0){
			db.closeConnection();
			flag = false;
		}else{
			db.closeConnection();
			flag = true;
		}
		
		return flag;
	}
	
	public Review consult(Object field) throws SQLException{
		
		Connection con = db.openConnection();
		Review r = null;
		
		if(field instanceof Integer){
			int id = ((Integer) field).intValue();
			String query = "SELECT * FROM review WHERE idReview = ?";
			ps = con.prepareStatement(query);
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				r = new Review(rs.getString("text"),rs.getInt("Product_idProduct"),rs.getInt("Client_idClient"));
				r.setId(rs.getInt("idReview"));
			}
			
		}else if(field instanceof String){
			
			String text = field.toString();
			String query = "SELECT * FROM review WHERE text = ?";
			ps = con.prepareStatement(query);
			ps.setString(1, text);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()){
				r = new Review(rs.getString("text"),rs.getInt("Product_idProduct"),rs.getInt("Client_idClient"));
				r.setId(rs.getInt("idReview"));
			}
		}
		
		db.closeConnection();
		return r;
	}
	
	public Review consult(int idClient, int idProduct) throws SQLException{
		
		Connection con = db.openConnection();
		Review r = null;
		
		
		String query = "SELECT * FROM review WHERE client_idClient = ? AND Product_idProduct = ?";
		ps = con.prepareStatement(query);
		ps.setInt(1, idClient);
		ps.setInt(2, idProduct);
		ResultSet rs = ps.executeQuery();
		
		if(rs.next()){
			r = new Review(rs.getString("text"),rs.getInt("Product_idProduct"),rs.getInt("Client_idClient"));
			r.setId(rs.getInt("idReview"));
		}
		
		return r;
	}
	
	public List getAll() throws SQLException{
		
		ArrayList l = new ArrayList();
		Connection con = db.openConnection();
		String query = "SELECT * FROM review;";
		ps = con.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		
		while(rs.next()){
			Review r = new Review(rs.getString("text"),rs.getInt("Product_idProduct"),rs.getInt("Client_idClient"));
			r.setId(rs.getInt("idReview"));
			l.add(r);			
		}
		
		db.closeConnection();
		return l;
	}
	
	public boolean remove(Object field) throws SQLException{
		
		boolean flag = false;
		Connection con = db.openConnection();
		
		if(field instanceof Integer){
			int id = ((Integer) field).intValue();
			if(!consult(id).equals(null)){
				String query = "DELETE FROM review WHERE idReview = ?;";
				ps = con.prepareStatement(query);
				ps.setInt(1, id);
				ps.execute();
				flag = true;
			}	
		}
		
		db.closeConnection();
		return flag;
	}
	
	public boolean edit(Review r, String text, int idProduct) throws SQLException{
		
		boolean flag = false;
		Connection con = db.openConnection();
		String query = "UPDATE review SET text = ?, Product_idProduct = ? WHERE idReview = ?;";
		
		if(!consult(r.getId()).equals(null)){
			ps = con.prepareStatement(query);
			ps.setString(1, text);
			ps.setInt(2, idProduct);
			ps.setInt(3, r.getId());
			ps.executeUpdate();
			if(ps.getUpdateCount() > 0){
				flag = true;
			}
		}
		
		db.closeConnection();
		return flag;
	}
	
}
