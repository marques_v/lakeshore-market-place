package luc.product.dao;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

import luc.persistence.Dao;
import luc.persistence.DataBase;
import luc.product.model.Product;

public class ProductDao implements Dao {

	private Connection connection;
	
	public ProductDao() throws IOException{
		DataBase db = new DataBase();
		connection = db.openConnection();
	}
	
	@Override
	public boolean insert(Object o) throws SQLException {
        
		if(!o.getClass().equals(Product.class)){
			return false;
		}
		Product product = (Product) o;
		String sql = "insert into Product (name, price, Partner_idPartner, quantity) values (?,?,?,?);";;
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ps.setString(1, product.getName());
		ps.setFloat(2, product.getPrice());
		ps.setInt(3, product.getPartnerIdPartner());
		ps.setInt(4, product.getQuantity());
		
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public boolean edit(Object o) throws SQLException {
		String sql = "update Product set name=?,price=?, quantity=? where idProduct=?;";
		
		if(!o.getClass().equals(Product.class)){
			return false;
		}
		
		Product product = (Product) o;
		
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		ps.setString(1, product.getName());
		ps.setFloat(2, product.getPrice());
		ps.setInt(3, product.getQuantity());
		ps.setInt(4, product.getIdProduct());
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public boolean remove(Object o) throws SQLException {
		Integer id = (Integer) o;
		
		String sql = "delete from Product where idProduct=? limit 1;";
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		ps.setInt(1, id);
		
		return ps.executeUpdate()!=0;
	}

	@Override
	public Object consult(Object o) throws SQLException {
		String sql;
		PreparedStatement ps; 
		
		// This method only accepts Strings as name or the idProduct itself 
		if(o.getClass().equals(String.class)){
			String key_value = (String) o;
			sql = "select * from product where name=?;";
			ps = (PreparedStatement) connection.prepareStatement(sql);
			ps.setString(1, key_value);
		} else if(o.getClass().equals(Integer.class)){
			Integer key_value = (Integer) o;
			sql = "select * from product where idProduct=?;";
			ps = (PreparedStatement) connection.prepareStatement(sql);
			ps.setInt(1, key_value);
		} else{
			return null;
		}
		
		ResultSet rs = ps.executeQuery();
		rs.next();
		
		Product product = new Product();
		product.setName(rs.getString("name"));
		product.setIdProduct(rs.getInt("idProduct"));
		product.setPrice(rs.getFloat("price"));
		product.setPartnerIdPartner(rs.getInt("Partner_idPartner"));
		product.setQuantity(rs.getInt("quantity"));
		
		return product;
	}

	@Override
	public List getAll() throws SQLException {
		String sql = "select * from product;";
		
		PreparedStatement ps = (PreparedStatement) connection.prepareStatement(sql);
		
		if(!ps.execute()){
			return null;
		}
		
		ResultSet rs = ps.executeQuery();	
		
		ArrayList<Product> products = new ArrayList<Product>();
		while(rs.next()){
			Product p = new Product();
			p.setName(rs.getString("name"));
			p.setIdProduct(rs.getInt("idProduct"));
			p.setPrice(rs.getFloat("price"));
			p.setPartnerIdPartner(rs.getInt("Partner_idPartner"));
			p.setQuantity(rs.getInt("quantity"));
			
			products.add(p);
		}
		
		return products;
				
	}

}
