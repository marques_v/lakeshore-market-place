package luc.tests;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import luc.persistence.DataBase;

public class TestConnection {

	private DataBase db;
	
	@Before
	public void setUp() throws Exception{
		db = new DataBase();
	}
	
	@After
	public void tearDown() throws Exception{
		db = null;
	}
	
	@Test
	public void testGettingData(){
		assertEquals("com.mysql.jdbc.Driver", db.getDriver());
	}
	
	@Test
	public void testConnection(){
		db.openConnection();
	}
	
}
