package luc.tests;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import luc.address.dao.AddressDao;
import luc.address.model.Address;
import luc.customer.dao.ClientDao;
import luc.customer.model.Client;
import luc.persistence.DataBase;

public class TestCostumerRegistration {
	private DataBase db;
	private Client a,b,c;
	private ClientDao cd;
	private Date date1 = new Date(12/03/1992);
	private Date date2 = new Date(13/04/2009);
	private Address address = new Address("N. Winthrop","","Chicago","Il","USA");
	
	@Before
	public void setUp() throws IOException, SQLException{
		AddressDao ad = new AddressDao();
		db = new DataBase();
		ad.insert(address);
		address = ad.consult("N. Winthrop");
		a = new Client("daniel",date1,"sulman","123",address);
		b = new Client("pedro",date2,"peu","456",address);
		c = new Client("jose",date2,"ze","567",address);
		this.cd = new ClientDao();
	}
	@After
	public void tearDown(){
		db = null;
	}
	
	@Test
	public void insert(){
		
		try{
			assertEquals(true,cd.insert(a)&&cd.insert(b));
		}
		catch(SQLException | IOException e){
			e.printStackTrace();
		}
		
	}
	@Test
	public void edit(){
		try{
			cd.insert(a);
			a = cd.consult(a.getUserName());
			assertEquals(true,cd.edit(a, "daniel",date1,"sulman","123",address));
		}
		catch(SQLException | IOException e){
			e.printStackTrace();
		}
		
	}
	@Test
	public void  remove(){
		try{
			cd.insert(a);
			assertEquals(true,cd.remove(a.getUserName()));
		}
		catch(SQLException | IOException e){
			e.printStackTrace();
		}
	}
	
}
