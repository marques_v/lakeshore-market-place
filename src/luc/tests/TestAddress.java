package luc.tests;

import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import luc.address.dao.AddressDao;
import luc.address.model.Address;
import luc.persistence.DataBase;

public class TestAddress {

	private DataBase db;
	private Address a;
	private Address b;
	private AddressDao cd;
	
	@Before
	public void setUp() throws Exception{
		db = new DataBase();
		a = new Address("N Winthrop Ave","60660","Chicago","Illinois","US");
		b = new Address("W Sheridan","66666","Chicago","Illinois","US");
		this.cd = new AddressDao();
	}
	
	@After
	public void tearDown() throws Exception{
		db = null;
	}
	
	@Test
	public void insert(){
		try {
			assertEquals(true,cd.insert(a)&&cd.insert(b));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void consult(){
		try {
			assertNotEquals(null,cd.consult(13));
			assertNotEquals(null,cd.consult(b.getAddressInfo1()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@Test
	public void getAll(){
		try {
			assertNotEquals(0,cd.getAll().size());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void remove(){
		try {
			a.setIdAddress(cd.consult(a.getAddressInfo1()).getIdAddress());
			assertEquals(true,cd.remove(new Integer(a.getIdAddress()))&&cd.remove(b.getAddressInfo1()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void edit(){
		try {
			b.setIdAddress(cd.consult(b.getAddressInfo1()).getIdAddress());
			assertEquals(true,cd.edit(b,b.getAddressInfo1(), b.getAddressInfo2(), b.getCity(), "IL", b.getCountry()));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
