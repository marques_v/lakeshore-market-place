package luc.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.SQLException;

import luc.persistence.DataBase;
import luc.product.dao.ReviewDao;
import luc.product.model.Review;

public class TestReview {

	private DataBase db;
	private Review a;
	private Review b;
	private ReviewDao rd;
	
	@Before
	public void setUp() throws Exception{
		db = new DataBase();
		this.a = new Review("Muito bom, fera!",1,6);
		this.b = new Review("Do caralho!",1,6);
		this.rd = new ReviewDao();
	}
	
	@After
	public void tearDown() throws Exception{
		db = null;
	}
	
	@Test
	public void insert(){
		try {
			assertEquals(true,rd.insert(a)&&rd.insert(b));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void consult(){
		try {
			assertNotEquals(null,rd.consult(new Integer(5)));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getAll(){
		try {
			assertNotEquals(0,rd.getAll().size());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void remove(){
		try {
			assertEquals(true,rd.remove(new Integer(3)));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void edit(){
		a.setId(6);
		try {
			assertEquals(true,rd.edit(a, "parab�ns", 1));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
