package luc.tests;

import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import luc.product.business.ProductManager;
import luc.product.model.Product;

public class TestProductManager {
	private ProductManager pmanager;

	@Before
	public void setUp() throws Exception{
		pmanager = new ProductManager();
	}
	
	@After
	public void tearDown() throws Exception{
		pmanager = null;
	}
	
	@Test
	public void testGetPartnerProducts() throws SQLException{
		ArrayList<Product> products = pmanager.getPartnerProducts(2);
		assertTrue(products.size()==2);
	}
}
