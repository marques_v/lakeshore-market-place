package luc.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.mysql.jdbc.Connection;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import luc.customer.dao.ClientDao;
import luc.customer.model.Client;
import luc.persistence.DataBase;
import luc.product.business.ReviewManager;
import luc.product.dao.ProductDao;
import luc.product.dao.ReviewDao;
import luc.product.model.Product;
import luc.product.model.Review;

public class TestReviewManager {

	private DataBase db;
	private PreparedStatement ps;
	ReviewManager rm;
	ClientDao cd;
	
	@Before
	public void setUp() throws Exception{
		db = new DataBase();
		this.cd = new ClientDao();
		this.rm = new ReviewManager();
		
	}
	
	@After
	public void tearDown() throws Exception{
		db = null;
	}
	
	@Test
	public void addReview(){	
		
		try {
			ProductDao pd = new ProductDao();
			Product p = (Product) pd.consult("brigadeiro");
			assertEquals(true,rm.addReview(cd.consult("admin"),p,"Parab�ns"));
		} catch (IOException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void editReview() throws SQLException{
		try {
			ReviewDao rd = new ReviewDao();
			Connection con = db.openConnection();
			String query = "SELECT * FROM review WHERE idReview = (SELECT MIN(idReview) FROM review);";
			ps = con.prepareStatement(query);
			ResultSet rs = ps.executeQuery();
			rs.next();
			int idReview = rs.getInt("idReview");
			Review r = rd.consult(idReview);
			r.setText("Changed");
			db.closeConnection();
			assertEquals(true,rm.editReview(r));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	@Test
	public void removeReview() throws IOException, SQLException{
		ReviewDao rd = new ReviewDao();
		Connection con = db.openConnection();
		String query = "SELECT MAX(idReview) id FROM review;";
		ps = con.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		rs.next();
		int idReview = rs.getInt("id");	
		db.closeConnection();
		assertEquals(true,rm.removeReview(idReview));
	}
	
	@Test
	public void getReviewsProduct() throws SQLException, IOException{
		
		Connection con = db.openConnection();
		ProductDao pd = new ProductDao();
		String query = "SELECT MIN(idProduct) id FROM product;";
		ps = con.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		rs.next();
		int idProduct = rs.getInt("id");	
		db.closeConnection();
		ArrayList<Review> l = rm.getReviewsProduct(idProduct);
		boolean flag = true;
		for(Review r : l){
			if(r.getIdProduct() != idProduct){
				flag = false;
				break;
			}				
		}
		
		assertEquals(true,flag);
		
	}
	
	@Test
	public void getReviewsClient() throws SQLException, IOException{
		
		Connection con = db.openConnection();
		ClientDao cd = new ClientDao();
		String query = "SELECT MIN(idClient) id FROM client;";
		ps = con.prepareStatement(query);
		ResultSet rs = ps.executeQuery();
		rs.next();
		int idClient = rs.getInt("id");	
		db.closeConnection();
		ArrayList<Review> l = rm.getReviewsClient(idClient);
		boolean flag = true;
		for(Review r : l){
			if(r.getIdClient() != idClient){
				flag = false;
				break;
			}				
		}
		
		assertEquals(true,flag);
		
	}
	
	
	
	/*
	query = "SELECT MIN(idClient) id FROM client;";
	ps = con.prepareStatement(query);
	rs = ps.executeQuery();
	rs.next();
	int idClient = rs.getInt("id");
	*/
}
