package luc.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import luc.order.dao.PaymentDao;
import luc.order.model.Payment;
import luc.persistence.DataBase;

public class TestPayment {

	private DataBase db;
	private Payment a;
	private Payment b;
	private PaymentDao pd;
	
	
	@Before
	public void setUp() throws Exception{
		db = new DataBase();
//		a = new Payment(Date.valueOf(LocalDate.now()),(float) 100,1);
//		b = new Payment(Date.valueOf(LocalDate.now()),(float) 200,1);
		this.pd = new PaymentDao();
	}
	
	@After
	public void tearDown() throws Exception{
		db = null;
	}
	
	@Test
	public void insert(){
		try {
			assertEquals(true,pd.insert(a)&&pd.insert(b));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void consult(){
		try {
			assertNotEquals(null,pd.consult(3));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getAll(){
		try {
			assertNotEquals(null,pd.getAll());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	@Test
	public void remove(){
		try {
			assertEquals(true,pd.remove(3));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void edit(){
//		try {
//			b.setIdPayment(4);
//			assertEquals(true,pd.edit(b, b.getDate(), 300, b.getBuyOrderIdBuyOrder()));
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
	}
	
}
