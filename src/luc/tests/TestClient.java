package luc.tests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;

import luc.address.dao.AddressDao;
import luc.address.model.Address;
import luc.customer.builder.AbstractClientBuilder;
import luc.customer.builder.ClientBuilder;
import luc.customer.builder.ClientDirector;
import luc.customer.dao.ClientDao;
import luc.customer.model.Client;
import luc.persistence.DataBase;

public class TestClient {

	private DataBase db;
	private Client a;
	private Client b;
	Address aa;
	Address ab;
	private ClientDirector director;
	private AbstractClientBuilder builder;
	private ClientDao cd;
	private AddressDao ad;
	
	@Before
	public void setUp() throws Exception{
		db = new DataBase();
		director = new ClientDirector(new ClientBuilder());
		ad = new AddressDao();
		this.aa = ad.consult("N Winthrop Ave");
		this.ab = ad.consult("W Sheridan");
		this.a = director.buildClient("fulano", Date.valueOf(LocalDate.now()), "root", "root",aa);
		this.b = director.buildClient("ciclano", Date.valueOf(LocalDate.now()), "admin", "admin",ab);
		this.cd = new ClientDao();
	}
	
	@After
	public void tearDown() throws Exception{
		db = null;
	}
	
	@Test
	public void consult(){
		try {
			assertNotEquals(null,cd.consult(new Integer(7)));
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void insert(){
		try {
			assertEquals(true,cd.insert(a)&&cd.insert(b));
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
	
	
	@Test
	public void getAll(){
		try {
			assertNotEquals(0,cd.getAll().size());
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void remove(){
		try {
			assertEquals(true,cd.remove(a.getUserName()));
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void edit(){
		
		try {
			a.setIdClient(cd.consult(a.getUserName()).getIdClient());			
			assertEquals(true,cd.edit(a, "Jack", a.getMembershipDate(), "root", "123",ab));
		} catch (SQLException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
