package luc.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import luc.customer.dao.ClientDao;
import luc.customer.model.Client;
import luc.order.business.BuyOrderManager;
import luc.order.business.Status;
import luc.order.dao.BuyOrderDao;
import luc.order.model.BuyOrder;
import luc.product.dao.ProductDao;
import luc.product.model.Product;

public class TestBuyingExperience {
	private BuyOrderManager bom;
	private ProductDao product_dao;
	private BuyOrderDao bod;
	
	@Before
	public void setUp() throws Exception{
		bom = new BuyOrderManager();
		product_dao = new ProductDao();
		bod = new BuyOrderDao();
	}
	
	@After
	public void tearDown() throws Exception{
		bom = null;
		product_dao = null;
		bod = null;
	}
	
	@Test
	public void buy() throws Exception{
		ClientDao client_dao = new ClientDao();
		Client client = client_dao.consult("henri");
		// Search producrt
		Product product = (Product) product_dao.consult("Carioca Bean");
		assertTrue(product!=null);
		// Check Aval
		int quantity_to_buy = 48;
		assertTrue(product.getQuantity()>quantity_to_buy);
		
		// buy
		float total = product.getPrice()*quantity_to_buy;
		int paymentNumberFixed = 4;
		String estimatedArrival = "2015/10/04";
		assertTrue(bom.buy(product.getIdProduct(), quantity_to_buy, client.getIdClient(), total, paymentNumberFixed, estimatedArrival) != null);
	}
	
	@Test
	public void pay() throws Exception{
//		BuyOrder buy_order = (BuyOrder) bod.consult(bod.getLastId());
		assertTrue(bom.payment(bod.getLastId()));
	}
	
	@Test
	public void changeStatus() throws Exception{
		BuyOrder buy_order = (BuyOrder) bod.consult(bod.getLastId());
		buy_order.setStatus(Status.ACTIVE);
		bod.edit(buy_order);
	}
	
//	@Test
//	public void cancelOrder() throws Exception{
//		BuyOrder buy_order = (BuyOrder) bod.consult(1);
//		assertTrue(bom.cancelOrder(buy_order));
//	}

}
